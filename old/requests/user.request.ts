import { _Request } from '../karrot/decorators/requests.decorator';

@_Request({
    name: "user",
    request_file: "php/user.php",
    request_params: ['id']
})
export class UserRequest {
    send_request:Function;
}