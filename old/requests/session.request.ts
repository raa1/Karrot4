import { _Request } from '../karrot/decorators/requests.decorator';

@_Request({
    name: "session",
    request_file: "php/session.php",
    request_params: ['sessionId']
})
export class SessionRequest {
    send_request:Function;
}