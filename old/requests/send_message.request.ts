import { _Request } from '../karrot/decorators/requests.decorator';

@_Request({
    name: "send_message",
    request_file: "php/send.php",
    request_params: ["user_id", "channel_id", "content"]
})
export class SendMessageRequest {
    send_request:Function;
}