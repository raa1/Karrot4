import { _Request } from '../karrot/decorators/requests.decorator';

@_Request({
    name: "channels",
    request_file: "php/channels_list.php",
    request_params: []
})
export class ChannelsRequest {
    send_request:Function;
}