import { _Request } from '../karrot/decorators/requests.decorator';

@_Request({
    name: "users_activity",
    request_file: "php/users_activity.php",
    request_params: ['sessionId', 'activeTab', 'channelId']
})
export class UsersActivityRequest {
    send_request:Function;
}