import { _Request } from '../karrot/decorators/requests.decorator';

@_Request({
    name: "login",
    request_file: "php/login.php",
    request_params: ['login', 'password']
})
export class LoginRequest {
    send_request:Function;
}