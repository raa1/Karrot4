import { _Request } from '../karrot/decorators/requests.decorator';

@_Request({
    name: "messages",
    request_file: "php/messages.php",
    request_params: ["last_id", "channel_id"]
})
export class MessagesRequest {
    send_request:Function;
}