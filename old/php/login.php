<?php
    require_once('config.php');
   
    $login = $_POST['login'];
    $password = $_POST['password'];
   
    $query = $db->prepare('SELECT COUNT(*) FROM users WHERE login = :login AND password = :password');
    $query->bindParam(':login', $login, PDO::PARAM_STR, 60);
    $query->bindParam(':password', $password, PDO::PARAM_STR, 60);
    $query->execute();
    $query = $query->fetchColumn();
   
    if (!$query) {
        echo 0;
        return;
    }
    session_start();
    $sessionId = session_id();
   
    $query = $db->prepare('UPDATE users SET session = :sessionId WHERE login = :login');
    $query->bindParam(':sessionId', $sessionId, PDO::PARAM_STR);
    $query->bindParam(':login', $login, PDO::PARAM_STR);
    $query->execute();
   
    echo $sessionId;
   
?>