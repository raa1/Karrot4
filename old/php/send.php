<?php
    require_once('config.php');
   
    $userId = $_POST['user_id'];
    $channelId = $_POST['channel_id'];
    $content = stripslashes($_POST['content']);
    //echo 'elo '.$userId.', '.$channelId.', '.$content;
   
    $query = $db->prepare('INSERT INTO messages (user_id, channel_id, data, content) VALUES (:userId, :channelId, CURRENT_TIMESTAMP, :content)');
    $query->bindParam(':userId', $userId, PDO::PARAM_INT);
    $query->bindParam(':channelId', $channelId, PDO::PARAM_INT);
    $query->bindParam(':content', $content, PDO::PARAM_STR, 300);
    $query->execute();
?>