<?php 
    require_once('config.php'); 
    
    $Id = $_POST['id']; 

    $query = $db->prepare('SELECT * FROM users WHERE id = :id'); 
    $query->bindParam(':id', $Id, PDO::PARAM_INT);
    $query->execute(); 
    $query = $query->fetch(PDO::FETCH_ASSOC); 
    
    echo json_encode($query); 
?>