<?php
    require_once('config.php');
   $lastId = $_POST['last_id'];
   $channelId = $_POST['channel_id'];

   
    $query = $db->prepare('SELECT messages.id, users.login AS name, DATE_FORMAT(data, "%e %M %Y, %k:%i") AS data, content FROM messages INNER JOIN users ON users.id = messages.user_id WHERE channel_id = :channelId AND messages.id > :lastId ORDER BY messages.id DESC LIMIT 20 ');
    $query->bindParam(':lastId', $lastId, PDO::PARAM_INT);
    $query->bindParam(':channelId', $channelId, PDO::PARAM_INT);
    $query->execute();

    $messages = $query->fetchAll(PDO::FETCH_ASSOC);
   
    $messages = array_reverse($messages);
    //$messages = strip_tags($messages);
    for ($x = 0; $x < count($messages); $x++)
        $messages[$x]['content'] = htmlspecialchars($messages[$x]['content']);

    echo json_encode($messages);
?>