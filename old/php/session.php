<?php 
    require_once('config.php'); 
    
    $sessionId = $_POST['sessionId']; 
    
    $query = $db->prepare('UPDATE users SET last_activity = CURRENT_TIMESTAMP WHERE session = :sessionId');
    $query->bindParam(':sessionId', $sessionId, PDO::PARAM_STR);
    $query->execute();

    $query = $db->prepare('SELECT id, login FROM users WHERE session = :sessionId'); 
    $query->bindParam(':sessionId', $sessionId, PDO::PARAM_STR); 
    $query->execute(); 
    $query = $query->fetch(PDO::FETCH_ASSOC); 
    
    
    
    echo json_encode($query); 
?>