<?php 
    require_once('config.php'); 
    
    $sessionId = $_POST['sessionId']; 
    $active_tab = $_POST['activeTab']; 
    $channel_id = $_POST['channelId']; 
    
    $query = $db->prepare('UPDATE users SET last_activity = CURRENT_TIMESTAMP, last_channel = :channelId, active_tab = :activeTab  WHERE session = :sessionId');
    $query->bindParam(':sessionId', $sessionId, PDO::PARAM_STR);
    $query->bindParam(':activeTab', $active_tab, PDO::PARAM_BOOL);
    $query->bindParam(':channelId', $channel_id, PDO::PARAM_INT);
    $query->execute();

    $query = $db->prepare('SELECT login, UNIX_TIMESTAMP(last_activity) as last_active, last_channel, active_tab  FROM users'); 
    $query->execute(); 
    $query = $query->fetchAll(PDO::FETCH_ASSOC); 
    
    echo json_encode($query); 
?>