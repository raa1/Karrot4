import { _RoutingHandler } from '../karrot/decorators/routing_handler.decorator';

import { UserStorage } from '../storages/user.storage';

@_RoutingHandler({
    template_url: 'templates/user.html',
    storages: [UserStorage]
})
export class UserRoute {
    constructor(public id:number, public user_storage:UserStorage) {     
        console.log('id', this.id, this.user_storage);  
    }

    on_load() {

    }
}