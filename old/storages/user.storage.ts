import { _Storage } from '../karrot/decorators/storages.decorator';

import { SendMessageRequest } from '../requests/send_message.request';
import { LoginRequest } from '../requests/login.request';
import { SessionRequest } from '../requests/session.request';


import { _Watchable } from '../karrot/decorators/watcher.decorator';
@_Storage({
    name: "user_storage",
    requests: [LoginRequest, SessionRequest, SendMessageRequest]
})
@_Watchable()
export class UserStorage {
    public id:Number = 0;
    public login:String = '';
    public session:string = '';
    public channel_id = 1;
    public active_tab = 1;

    constructor(public login_request:LoginRequest,  public session_request:SessionRequest, public send_message_request:SendMessageRequest) {
    }
    
    on_load() {
        this.session = sessionStorage.getItem('session');
        this.session_login();

        window.onfocus =  () => { 
            this.active_tab = 1; 
        }; 
        window.onblur = () => { 
            this.active_tab = 0; 
        }; 
    }

    //Login things
    login_from_form(username, password) {
        this.login_request.send_request([username, password]).then((data) => {
            this.session = data;
            this.session_login();   
        });
    }
    session_login() {
        if(this.session != null) {
            this.session_request.send_request([this.session]).then((data) => {
                let json_data = JSON.parse(data);
                if(json_data.id != undefined) {
                    this.id = json_data.id;
                    this.login = json_data.login;
                    sessionStorage.setItem('session', this.session);
                    console.log('session success?');
                    this.login_success('test');
                }
            })
        }
    }
    //Watched method
    login_success(a) {
        this.routing.route_to('chat');
        return '';
    }


    write_message(content) {
        this.send_message_request.send_request([this.id, this.channel_id, content]).then(() => {

        });
    }

    logout() {
        sessionStorage.setItem('session', '');
        location.reload();
    }


}