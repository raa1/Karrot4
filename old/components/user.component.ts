import { Component } from '../karrot/decorators/component.decorator';
import { UserRequest } from '../requests/user.request';

@Component({
    name: "user",
    selector: "#user",
    requests: [UserRequest]
})
export class User {
    constructor(public element:HTMLElement, public user_request:UserRequest) {
        
    }

    on_load() {
        console.log('user loaded');
        console.log(this.user_request);
        this.user_request.send_request([this.router.id]).then((data) => {
            let json_data = JSON.parse(data);
            this.element.innerHTML = '<p>Hello '+json_data.login+'</p>';
        });
    }

}

