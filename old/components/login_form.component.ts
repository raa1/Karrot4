import { Component } from '../karrot/decorators/component.decorator';

import { UserStorage } from '../storages/user.storage';

import { _Watch } from '../karrot/decorators/watcher.decorator';

@Component({
    name: "login_form",
    selector: "#login",
    storages: [UserStorage]
})
export class LoginFormComponent {
    public messages:Array<any> = [];
    public last_id:Number = 0;
    constructor(public element:HTMLElement, public user_storage:UserStorage) {
        
    }

    on_load() {

    }


    submit_login(e) {
        e.preventDefault();

        let login = this.element.querySelector('#login-input').value,
            password = this.element.querySelector('#password-input').value;

        this.user_storage.login_from_form(login, password);
    }

    @_Watch(UserStorage, 'login_success')
    logged() {
        console.log('watch test');

    }

    logout_click() {
        this.user_storage.logout();
    }

    history_test() {
        var stateObj = { foo: "bar" };
        history.pushState(stateObj, "page 2", "bar.html");
        this.element.innerHTML += "??";
        window.onpopstate = function(event) {
            console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));
        };
    }

}