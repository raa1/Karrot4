import { Component } from '../karrot/decorators/component.decorator';

import { ChatStorage } from '../storages/chat.storage';
import { UserStorage } from '../storages/user.storage';

import { MessagesRequest } from '../requests/messages.request';
import { ChannelsRequest } from '../requests/channels.request';
import { UsersActivityRequest } from '../requests/users_activity.request';

import { _Watch } from '../karrot/decorators/watcher.decorator';

@Component({
    name: "chat",
    selector: "#chat",
    requests: [MessagesRequest, ChannelsRequest, UsersActivityRequest],
    storages: [ UserStorage]
})
export class ChatComponent {
    public messages:Array<any> = [];
    public last_id:Number = 0;
    public channels:Array<any> = [];
    public active_users:Array<any> = [];

    constructor(public element:HTMLElement, public messages_req:MessagesRequest, public channels_req:ChannelsRequest, public users_activity:UsersActivityRequest, public user_storage:UserStorage) {
    }

    on_load() {
        if(this.user_storage.id > 0) {
            this.init_chat();
        } else {
            this.routing.route_to('login');
        }
    }

    //Watch login_success from UserStorage
   
    init_chat() {
        
        this.element.style.display = "block";
        this.load_messages();
        this.load_channels();
        this.load_active_users();
        setInterval(() => {
            this.load_messages();
            this.load_active_users();
        }, 1000);
    }


    //Messages
    load_messages():any {
        this.messages = [];
        this.messages_req
        .send_request([this.last_id, 
                       this.user_storage.channel_id])
        .then((data) => {
            let json_data = JSON.parse(data),
                new_messages = [];
                
            if(json_data.length > 0) {
                for(let message_json of json_data) {
                    let message = new Message(message_json);
                    new_messages.push(message);
                }
                this.last_id = new_messages.slice(-1).pop().id;
                this.render_messages(new_messages);
            }      
        });
        
    }

    render_messages(new_messages) {
        let messages_element = this.element.querySelector('.messages');
        for(let message of new_messages) {
            messages_element.innerHTML += '<p>'+message.parsed_message+'</p>';
        }
        messages_element.scrollTop = messages_element.scrollHeight;
    }

    //Channels
    load_channels() {
        this.channels_req.send_request([]).then((data)=> {
            let json_data = JSON.parse(data);
            for(let channel of json_data) {
                this.channels.push(channel);
            }
            this.render_channels();
        });
    }
    render_channels() {
        let channels_container = this.element.querySelector('#channels');
        for(let channel of this.channels) {
            channels_container.innerHTML += '<p data-channel-id="'+channel.id+'" k:click="change_channel">'+channel.name+'</p>';
        }
        this.__karrot_recheck_events();
    }

    change_channel(e, channel_element:HTMLElement) {
        let channel_id = channel_element.getAttribute('data-channel-id');

        this.user_storage.channel_id = parseInt(channel_id);
        this.messages = [];
        this.last_id = 0;

        let messages_element = this.element.querySelector('.messages');
        messages_element.innerHTML = '';
        this.load_messages();
        this.load_active_users();

    }

    //Active users
    load_active_users() {
        this.users_activity
        .send_request([this.user_storage.session, 
                       this.user_storage.active_tab, 
                       this.user_storage.channel_id])
        .then((data)=> {
            let json_data = JSON.parse(data);
            this.active_users = [];
            for(let active of json_data) {
                let date = Math.floor(Date.now() / 1000),
                    last_time = date - active.last_active;
                
                if(last_time < 30) {
                    this.active_users.push(active);
                }
                this.render_active_users();

            }
        });
    }

    render_active_users() {
        let active_users_element = this.element.querySelector('#online');

        active_users_element.innerHTML = '';
        for(let online of this.active_users) {
            if(online.last_channel == this.user_storage.channel_id)
                active_users_element.innerHTML += '<p class='+(online.active_tab == 1 ? 'active' : 'inactive')+'>'+online.login+' </p>';
        }

    }

    //DOM Events
    send_message(e) {
        e.preventDefault();
        let message_content = this.element.querySelector('#message-text');
        this.user_storage.write_message(message_content.value);

        message_content.value = '';
    }


}

class Message {
    public id:Number;
    public data:String;
    public author:String;
    public content:String;
    constructor(json_object ) {
        this.id = json_object.id;
        this.data = json_object.data;
        this.author = json_object.name;
        this.content = json_object.content;
        
    }

    get parsed_message() {
        return this.data+' '+this.author+': '+this.content;
    }
}