import { Component } from '../karrot/decorators/component.decorator';


@Component({
    name: "test_me",
    selector: ".test",
    storages: []
})
export class TestRak {
    public test_count:number = 0;
    constructor(public element:HTMLElement) {
        
    }

    on_load() {
        console.log(this.__karrot_name+' loaded');
    }
    test_click() {
        this.test_count++;
        console.log('test click '+this.test_count);
    }
}