define('karrot/core/init',["require", "exports"], function (require, exports) {
    "use strict";
    function KarrotInit(element) {
        var app = Reflect.construct(element, []);
        app.__karrot_init();
    }
    exports.KarrotInit = KarrotInit;
});
define('karrot/helpers/brackets_helper',["require", "exports"], function (require, exports) {
    "use strict";
    var BracketsHelper = (function () {
        function BracketsHelper() {
        }
        BracketsHelper._brackets = function (text_string) {
            return text_string.match(this._brackets_regexp);
        };
        BracketsHelper._values = function (text_string) {
            return text_string.split(this._values_regexp);
        };
        //replacing/reversing quotes
        BracketsHelper._replace_quotes = function (text_string) {
            text_string = text_string.replace(this._quotes_regexp, '_karrot_text');
            return text_string;
        };
        BracketsHelper._reverse_quotes = function (text_string, quotes) {
            for (var quote in quotes) {
                text_string = text_string.replace(quote, quotes[quote]);
            }
            return text_string;
        };
        return BracketsHelper;
    }());
    BracketsHelper._brackets_regexp = /\{([^}]+)\}/gm;
    BracketsHelper._values_regexp = /\s*[{\+\-\*\/\}\(\)\]\[\,]\s*/gm;
    BracketsHelper._quotes_regexp = /".*?"|'.*?'/gm;
    exports.BracketsHelper = BracketsHelper;
});
define('karrot/core/declarations',["require", "exports"], function (require, exports) {
    "use strict";
    var Declarations = (function () {
        function Declarations() {
        }
        Declarations.init = function (declarations) {
            this.scopes = declarations.scopes ? declarations.scopes : 0;
            this.pipes = declarations.pipes ? declarations.pipes : 0;
        };
        return Declarations;
    }());
    Declarations.scopes = [];
    Declarations.pipes = [];
    exports.Declarations = Declarations;
});
define('karrot/core/objects/_binding_object',["require", "exports", "../../helpers/brackets_helper", "../declarations"], function (require, exports, brackets_helper_1, declarations_1) {
    "use strict";
    var BindingObject = (function () {
        function BindingObject(element, scopes) {
            this.element = element;
            this.scopes = [];
            this.pipes = {};
        }
        BindingObject.prototype._check = function (_expression, scopes) {
            var expression = _expression.replace('{', '').replace('}', ''), pipes_expressions = [], pipes = {};
            var split_exp = expression.split('|');
            if (split_exp) {
                expression = split_exp.shift();
                pipes_expressions = split_exp;
            }
            var text_string = brackets_helper_1.BracketsHelper._replace_quotes(expression), _values = brackets_helper_1.BracketsHelper._values(text_string);
            var _loop_1 = function (_pipe) {
                var __pipe = _pipe, __params = '';
                var split_pipe = _pipe.split(':');
                if (split_pipe) {
                    __pipe = split_pipe.shift().replace(/\s/, '');
                    __params = split_pipe[0];
                }
                var pipe = declarations_1.Declarations.pipes.filter(function (e) { return e.prototype._karrot_name == __pipe; })[0];
                if (pipe) {
                    pipes[__pipe] = { pipe: pipe, params: __params };
                }
                console.log('pipes', pipes, __pipe, declarations_1.Declarations.pipes);
            };
            for (var _i = 0, pipes_expressions_1 = pipes_expressions; _i < pipes_expressions_1.length; _i++) {
                var _pipe = pipes_expressions_1[_i];
                _loop_1(_pipe);
            }
            var _loop_2 = function (_value) {
                if (_value == '' || !isNaN(_value) || _value.indexOf('_karrot_text') == 0 || _value == 'e' || _value == 'event')
                    return "continue";
                var scope = _value.split('.')[0], valid_scopes = scopes.filter(function (item) {
                    return item._karrot_name == scope ? true : false;
                });
                if (valid_scopes.length == 0)
                    throw new Error('Invalid value "' + _value + '" in element:' + this_1.element);
                var id = valid_scopes[0]._karrot_id;
                if (this_1.scopes.indexOf(id) == -1)
                    this_1.scopes.push(id);
            };
            var this_1 = this;
            for (var _a = 0, _values_1 = _values; _a < _values_1.length; _a++) {
                var _value = _values_1[_a];
                _loop_2(_value);
            }
            return { expression: expression, pipes: pipes };
        };
        return BindingObject;
    }());
    exports.BindingObject = BindingObject;
});
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define('karrot/core/objects/attribute_binding',["require", "exports", "./_binding_object"], function (require, exports, _binding_object_1) {
    "use strict";
    var AttributeBinding = (function (_super) {
        __extends(AttributeBinding, _super);
        function AttributeBinding(element, scopes, attr_name, expression) {
            var _this = _super.call(this, element, scopes) || this;
            _this.element = element;
            _this.expression = expression;
            _this.last_value = '';
            var _attr_name = attr_name.replace(/\[|\]/gm, '');
            _this.attribute = _attr_name;
            _this._check(expression, scopes);
            return _this;
        }
        return AttributeBinding;
    }(_binding_object_1.BindingObject));
    exports.AttributeBinding = AttributeBinding;
});
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define('karrot/core/objects/attribute_condition_binding',["require", "exports", "./_binding_object"], function (require, exports, _binding_object_1) {
    "use strict";
    var AttributeConditionBinding = (function (_super) {
        __extends(AttributeConditionBinding, _super);
        function AttributeConditionBinding(element, scopes, attr_name, expression) {
            var _this = _super.call(this, element, scopes) || this;
            _this.element = element;
            _this.expression = expression;
            var _attr_name = attr_name.replace(/\(|\)/gm, '').split('.');
            _this.attribute = _attr_name[0];
            if (_attr_name[1].indexOf('|') > 0) {
                var _value = _attr_name[1].split('|');
                _this.value = _value[0];
                _this.false_value = _value[1];
            }
            else {
                _this.value = _attr_name[1];
            }
            _this._check(expression, scopes);
            console.log('dis', _this);
            return _this;
        }
        return AttributeConditionBinding;
    }(_binding_object_1.BindingObject));
    exports.AttributeConditionBinding = AttributeConditionBinding;
});
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define('karrot/core/objects/style_binding',["require", "exports", "./_binding_object", "../../helpers/brackets_helper"], function (require, exports, _binding_object_1, brackets_helper_1) {
    "use strict";
    var StyleBinding = (function (_super) {
        __extends(StyleBinding, _super);
        function StyleBinding(element, scopes, style) {
            var _this = _super.call(this, element, scopes) || this;
            _this.element = element;
            _this.style = style;
            _this.brackets = [];
            var brackets = brackets_helper_1.BracketsHelper._brackets(style);
            _this.original_value = style;
            for (var _i = 0, brackets_1 = brackets; _i < brackets_1.length; _i++) {
                var bracket = brackets_1[_i];
                _this.brackets.push(bracket);
                _this._check(bracket, scopes);
            }
            return _this;
        }
        return StyleBinding;
    }(_binding_object_1.BindingObject));
    exports.StyleBinding = StyleBinding;
});
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define('karrot/core/objects/event_binding',["require", "exports", "./_binding_object"], function (require, exports, _binding_object_1) {
    "use strict";
    var EventBinding = (function (_super) {
        __extends(EventBinding, _super);
        function EventBinding(element, scopes, attr_name, expression) {
            var _this = _super.call(this, element, scopes) || this;
            _this.element = element;
            _this.expression = expression;
            var _attr_name = attr_name.replace('k:', 'on');
            _this.attribute = _attr_name;
            _this._check(expression, scopes);
            return _this;
        }
        return EventBinding;
    }(_binding_object_1.BindingObject));
    exports.EventBinding = EventBinding;
});
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define('karrot/core/objects/text_binding',["require", "exports", "./_binding_object", "../../helpers/brackets_helper"], function (require, exports, _binding_object_1, brackets_helper_1) {
    "use strict";
    var TextBinding = (function (_super) {
        __extends(TextBinding, _super);
        function TextBinding(node, scopes) {
            var _this = _super.call(this, node, scopes) || this;
            _this.node = node;
            _this.brackets = [];
            _this.original_value = node.nodeValue;
            var brackets = brackets_helper_1.BracketsHelper._brackets(_this.node.nodeValue);
            for (var _i = 0, brackets_1 = brackets; _i < brackets_1.length; _i++) {
                var bracket = brackets_1[_i];
                var _a = _this._check(bracket, scopes), expression = _a.expression, pipes = _a.pipes;
                _this.brackets.push(new Bracket(expression, pipes, bracket));
            }
            return _this;
        }
        return TextBinding;
    }(_binding_object_1.BindingObject));
    exports.TextBinding = TextBinding;
    var Bracket = (function () {
        function Bracket(expression, pipes, original_value) {
            this.expression = expression;
            this.pipes = pipes;
            this.original_value = original_value;
        }
        return Bracket;
    }());
});
//pipe
//params ;
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define('karrot/core/objects/visible_condition',["require", "exports", "./_binding_object"], function (require, exports, _binding_object_1) {
    "use strict";
    var VisibleCondition = (function (_super) {
        __extends(VisibleCondition, _super);
        function VisibleCondition(element, scopes, expression) {
            var _this = _super.call(this, element, scopes) || this;
            _this.element = element;
            _this.expression = expression;
            _this._check(expression, scopes);
            return _this;
        }
        return VisibleCondition;
    }(_binding_object_1.BindingObject));
    exports.VisibleCondition = VisibleCondition;
});
define('karrot/helpers/unique_value',["require", "exports"], function (require, exports) {
    "use strict";
    var UniqueValue = (function () {
        function UniqueValue() {
        }
        Object.defineProperty(UniqueValue, "UniqueStorage", {
            get: function () {
                this.unique_storage_val++;
                return '__karrot_storage_' + this.unique_storage_val;
            },
            enumerable: true,
            configurable: true
        });
        return UniqueValue;
    }());
    UniqueValue.unique_storage_val = 0;
    exports.UniqueValue = UniqueValue;
});
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
define('karrot/core/data_binding',["require", "exports", "./objects/attribute_binding", "./objects/attribute_condition_binding", "./objects/style_binding", "./objects/event_binding", "./objects/text_binding", "./objects/visible_condition", "../helpers/unique_value", "../helpers/brackets_helper", "./declarations"], function (require, exports, attribute_binding_1, attribute_condition_binding_1, style_binding_1, event_binding_1, text_binding_1, visible_condition_1, unique_value_1, brackets_helper_1, declarations_1) {
    "use strict";
    /**
     * element: asd
     * scopes: []
     * brackets;
     *
     */
    var DataBinding = (function () {
        function DataBinding(data_element, scopes, services, router) {
            if (router === void 0) { router = undefined; }
            this.data_element = data_element;
            this.scopes = scopes;
            this.services = services;
            this.router = router;
            this.children = [];
            this.text_bindings = [];
            this.event_bindings = [];
            this.attribute_bindings = [];
            this.attribute_con_bindings = [];
            this.style_bindings = [];
            this.used_scopes = [];
            this.is_visible = true;
        }
        Object.defineProperty(DataBinding.prototype, "main_data_binding", {
            get: function () { return !this.parent ? this : this.parent.main_data_binding; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataBinding.prototype, "local_scopes", {
            get: function () {
                var _this = this;
                return this.scopes.filter(function (s) { return _this.parent ? _this.parent.scopes.indexOf(s) == -1 ? true : false : true; });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataBinding.prototype, "dynamic_scopes", {
            get: function () { return this.scopes.filter(function (s) { return s.is_dynamic ? true : false; }); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataBinding.prototype, "local_dynamic_scopes", {
            get: function () { return this.local_scopes.filter(function (s) { return s.is_dynamic ? true : false; }); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataBinding.prototype, "binding_objects", {
            get: function () { return this.attribute_bindings.concat(this.attribute_con_bindings, this.event_bindings, this.text_bindings, this.style_bindings); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataBinding.prototype, "children_used_scope", {
            get: function () {
                var used_scopes = this.used_scopes;
                for (var _a = 0, _b = this.children; _a < _b.length; _a++) {
                    var child = _b[_a];
                    if (child.is_visible)
                        used_scopes = used_scopes.concat(child.children_used_scope.filter(function (e) { return used_scopes.indexOf(e) == -1; }));
                }
                return used_scopes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataBinding.prototype, "local_services", {
            get: function () {
                var _this = this;
                return this.parent ? this.services.filter(function (s) { return _this.parent.services.indexOf(s) == -1 ? true : false; }) : this.services;
            },
            enumerable: true,
            configurable: true
        });
        DataBinding.prototype.init = function () {
            var _this = this;
            var promise = new Promise(function (resolve, reject) {
                var children_promise = [];
                _this._parse();
                _this.parse_used_scopes();
                _this._eval_events();
                _this.init_services().then(function () {
                    _this._eval_texts();
                    _this._eval_attrs();
                    _this._eval_con_attrs();
                    _this._eval_styles();
                    _this.eval_visible_condition();
                    for (var _a = 0, _b = _this.children; _a < _b.length; _a++) {
                        var child = _b[_a];
                        children_promise.push(child.init());
                    }
                    Promise.all(children_promise).then(function () {
                        resolve();
                    });
                });
            });
            return promise;
        };
        DataBinding.prototype.init_services = function () {
            var _this = this;
            var promise = new Promise(function (resolve, reject) {
                var services_load = [];
                var _loop_1 = function (service) {
                    if (service.load_data) {
                        var load_data = service.load_data();
                        load_data.then(function () {
                            if (service.init)
                                service.init();
                        });
                        services_load.push(load_data);
                    }
                    else if (service.init) {
                        service.init();
                    }
                };
                for (var _a = 0, _b = _this.local_services; _a < _b.length; _a++) {
                    var service = _b[_a];
                    _loop_1(service);
                }
                var _loop_2 = function (scope) {
                    if (scope.load_data) {
                        var load_data = scope.load_data();
                        load_data.then(function () {
                            if (scope.init)
                                scope.init();
                        });
                        services_load.push(load_data);
                    }
                    else if (scope.init) {
                        scope.init();
                    }
                };
                for (var _c = 0, _d = _this.local_scopes; _c < _d.length; _c++) {
                    var scope = _d[_c];
                    _loop_2(scope);
                }
                Promise.all(services_load).then(function () {
                    resolve();
                });
            });
            return promise;
        };
        //parse block
        DataBinding.prototype._parse = function () {
            this._parse_nodes(this.data_element);
        };
        //Loop method, parse all nodes, starts with data_element.
        DataBinding.prototype._parse_nodes = function (node) {
            //Check is #text node, then create TextNode
            if (node.nodeName == '#text') {
                //if empty/no brackets, skip
                if (!node.nodeValue.match(/\S/) ||
                    !brackets_helper_1.BracketsHelper._brackets(node.nodeValue))
                    return;
                var text_binding = new text_binding_1.TextBinding(node, this.scopes);
                this.text_bindings.push(text_binding);
                return;
            }
            //if not text node, convert to element
            var element = node, attr_bindings = [], attr_con_bindings = [], attr_k = [], is_data_element = this.data_element == element;
            /*if(element.nodeName == 'K-ROUTE' && this.data_element != element) {
                this.create_route_child(element);
                return;
            }*/
            //check events/directives/new scopes in attr
            for (var i = 0; i < element.attributes.length; i++) {
                var attr = element.attributes[i];
                if (!is_data_element) {
                    if (attr.name.indexOf('*k:') == 0) {
                        switch (attr.name) {
                            case '*k:for':
                                this.create_loop_child(element, attr.value);
                                break;
                            case '*k:if':
                                var child = this.create_child(element);
                                child.visible_condition = new visible_condition_1.VisibleCondition(element, child.scopes, attr.value);
                                break;
                        }
                        return;
                    }
                    if (attr.name.indexOf('__karrot_scope_') == 0) {
                        var child = this.create_child(element), scope_name = attr.name.replace('__karrot_scope_', ''), instance = child.create_scope(scope_name);
                        child.add_scope(instance);
                        return;
                    }
                }
                if (attr.name.indexOf('k:') == 0)
                    attr_k.push(attr);
                if (attr.name.indexOf('[') == 0)
                    attr_bindings.push(attr);
                if (attr.name.indexOf('(') == 0)
                    attr_con_bindings.push(attr);
            }
            //check attributes bindings
            for (var _a = 0, attr_bindings_1 = attr_bindings; _a < attr_bindings_1.length; _a++) {
                var k = attr_bindings_1[_a];
                element.removeAttribute(k.name);
                var attribute_binding = new attribute_binding_1.AttributeBinding(element, this.scopes, k.name, k.value);
                if (k.name == '[value]') {
                    var event = new event_binding_1.EventBinding(element, this.scopes, 'k:keyup', k.value + '=e.target.value');
                    this.event_bindings.push(event);
                }
                this.attribute_bindings.push(attribute_binding);
            }
            //check attributes condition bindings
            for (var _b = 0, attr_con_bindings_1 = attr_con_bindings; _b < attr_con_bindings_1.length; _b++) {
                var k = attr_con_bindings_1[_b];
                element.removeAttribute(k.name);
                var attribute_con_binding = new attribute_condition_binding_1.AttributeConditionBinding(element, this.scopes, k.name, k.value);
                this.attribute_con_bindings.push(attribute_con_binding);
            }
            //check 'k:' attributes
            for (var _c = 0, attr_k_1 = attr_k; _c < attr_k_1.length; _c++) {
                var k = attr_k_1[_c];
                element.removeAttribute(k.name);
                switch (k.name) {
                    case 'k:style':
                        var style_binding = new style_binding_1.StyleBinding(element, this.scopes, k.value);
                        this.style_bindings.push(style_binding);
                        break;
                    default:
                        var event = new event_binding_1.EventBinding(element, this.scopes, k.name, k.value);
                        this.event_bindings.push(event);
                        break;
                }
            }
            /*if(attr.name.indexOf('router-link') == 0) {
                let href = attr.value;
                element.removeAttribute('router-link');
                element.setAttribute('href', href);
                window.onpopstate = (event) => {
                    let _element = this.router.route_data_binding.data_element;
                    this.router.route_data_binding.remove();
                    let child = this.create_route_child(_element);
                    child.init();
                };
                element.addEventListener('click', (e) => {
                    e.preventDefault();
                    this.router.route_to(href);
                    let _element = this.router.route_data_binding.data_element;
                    this.router.route_data_binding.remove();
                    let child = this.create_route_child(_element);
                    child.init();
                })
            }*/
            for (var _d = 0, _e = element.childNodes; _d < _e.length; _d++) {
                var childNode = _e[_d];
                this._parse_nodes(childNode);
            }
        };
        DataBinding.prototype.parse_used_scopes = function () {
            var used_scopes = [];
            for (var _a = 0, _b = this.binding_objects; _a < _b.length; _a++) {
                var binding_object = _b[_a];
                used_scopes = used_scopes.concat(binding_object.scopes.filter(function (e) { return used_scopes.indexOf(e) == -1; }));
            }
            console.log(this, used_scopes, this.binding_objects);
            this.used_scopes = used_scopes;
        };
        DataBinding.prototype.change_detect = function (used_scopes) {
            if (used_scopes === void 0) { used_scopes = []; }
            if (!this.children_used_scope.some(function (e) { return used_scopes.indexOf(e) > -1 || used_scopes.length == 0; }))
                return;
            this.eval_visible_condition();
            if (!this.is_visible)
                return;
            /*this.check_attribute_handlers();
            this.eval_styles();
            this.eval_text_nodes();*/
            this._eval_texts();
            this._eval_attrs();
            this._eval_con_attrs();
            this._eval_styles();
            for (var _a = 0, _b = this.children; _a < _b.length; _a++) {
                var child = _b[_a];
                child.change_detect(used_scopes);
            }
        };
        DataBinding.prototype.eval_scopes = function () {
            var eval_scope = '';
            for (var scope in this.scopes) {
                eval_scope += 'var ' + this.scopes[scope]._karrot_name + ' = scopes[' + scope + ']' + (this.scopes[scope].is_dynamic ? '.data' : '') + ';\n';
            }
            return eval_scope;
        };
        DataBinding.prototype._eval = function (expression) {
            var value = '', scopes = this.scopes;
            eval(this.eval_scopes() + ";\n            value = " + expression + ";\n        ");
            return value;
        };
        //uppercase.transform(10);
        //Declarations
        DataBinding.prototype._eval_texts = function (used_scopes) {
            if (used_scopes === void 0) { used_scopes = []; }
            var scopes = this.scopes, eval_scopes = this.eval_scopes();
            for (var _a = 0, _b = this.text_bindings; _a < _b.length; _a++) {
                var text_binding = _b[_a];
                if (!text_binding.scopes.some(function (e) { return used_scopes.indexOf(e) > -1 || used_scopes.length == 0; }))
                    continue;
                var original_value = text_binding.original_value, parsed_value = original_value;
                for (var _c = 0, _d = text_binding.brackets; _c < _d.length; _c++) {
                    var bracket = _d[_c];
                    var _bracket = bracket.expression, pipes = bracket.pipes;
                    eval("" + eval_scopes + ";\n                    _bracket = " + _bracket + ";\n                 ");
                    /*for(let pipe in bracket.pipes) {
                        console.log(pipe);
                        eval(``+eval_scopes+`;
                           _bracket = pipes['`+pipe+`'].pipe.transform(_bracket, `+pipes[pipe].params+`);
                       `);
                    }*/
                    parsed_value = parsed_value.replace(bracket.original_value, _bracket);
                }
                if (text_binding.node.nodeValue != parsed_value)
                    text_binding.node.nodeValue = parsed_value;
            }
        };
        DataBinding.prototype._eval_events = function (used_scopes) {
            var _this = this;
            if (used_scopes === void 0) { used_scopes = []; }
            var scopes = this.scopes, eval_scopes = this.eval_scopes();
            var _loop_3 = function (event) {
                var evaled_event;
                event.element[event.attribute] = function (e) {
                    eval(eval_scopes + ';var event = e;\nevaled_event=' + event.expression);
                    switch (typeof evaled_event) {
                        case 'object':
                            if (evaled_event.then) {
                                evaled_event.then(function (response) {
                                    if (response !== false) {
                                        _this.main_data_binding.change_detect(event.scopes);
                                    }
                                });
                                break;
                            }
                        case 'boolean':
                            if (evaled_event === false)
                                break;
                        default:
                            _this.main_data_binding.change_detect(event.scopes);
                    }
                    console.log(evaled_event);
                };
            };
            for (var _a = 0, _b = this.event_bindings; _a < _b.length; _a++) {
                var event = _b[_a];
                _loop_3(event);
            }
        };
        DataBinding.prototype._eval_attrs = function (used_scopes) {
            if (used_scopes === void 0) { used_scopes = []; }
            var scopes = this.scopes, eval_scopes = this.eval_scopes();
            for (var _a = 0, _b = this.attribute_bindings; _a < _b.length; _a++) {
                var attr_binding = _b[_a];
                if (!attr_binding.scopes.some(function (e) { return used_scopes.indexOf(e) > -1 || used_scopes.length == 0; }))
                    continue;
                var value = '';
                eval(eval_scopes + ";\n                value = " + attr_binding.expression + ";\n            ");
                if (attr_binding.last_value != value) {
                    switch (attr_binding.attribute) {
                        case 'class':
                            attr_binding.element.classList.remove(attr_binding.last_value);
                            attr_binding.element.classList.add(value);
                            break;
                        default:
                            attr_binding.element.setAttribute(attr_binding.attribute, value);
                            break;
                    }
                    attr_binding.last_value = value;
                }
            }
        };
        DataBinding.prototype._eval_con_attrs = function (used_scopes) {
            if (used_scopes === void 0) { used_scopes = []; }
            var scopes = this.scopes, eval_scopes = this.eval_scopes();
            for (var _a = 0, _b = this.attribute_con_bindings; _a < _b.length; _a++) {
                var attr_con_binding = _b[_a];
                if (!attr_con_binding.scopes.some(function (e) { return used_scopes.indexOf(e) > -1 || used_scopes.length == 0; }))
                    continue;
                var value = void 0;
                eval(eval_scopes + ";\n                value = " + attr_con_binding.expression + ";\n            ");
                if (attr_con_binding.last_value != value) {
                    if (value) {
                        switch (attr_con_binding.attribute) {
                            case 'class':
                                attr_con_binding.element.classList.remove(attr_con_binding.false_value);
                                attr_con_binding.element.classList.add(attr_con_binding.value);
                                break;
                            default:
                                attr_con_binding.element.setAttribute(attr_con_binding.attribute, attr_con_binding.value);
                                break;
                        }
                    }
                    else {
                        switch (attr_con_binding.attribute) {
                            case 'class':
                                attr_con_binding.element.classList.remove(attr_con_binding.value);
                                attr_con_binding.element.classList.add(attr_con_binding.false_value);
                                console.log('add false value');
                                break;
                            default:
                                attr_con_binding.element.setAttribute(attr_con_binding.attribute, attr_con_binding.false_value);
                                break;
                        }
                    }
                    attr_con_binding.last_value = value;
                }
            }
        };
        DataBinding.prototype._eval_styles = function (used_scopes) {
            if (used_scopes === void 0) { used_scopes = []; }
            var scopes = this.scopes, eval_scopes = this.eval_scopes();
            for (var _a = 0, _b = this.style_bindings; _a < _b.length; _a++) {
                var style_binding = _b[_a];
                if (!style_binding.scopes.some(function (e) { return used_scopes.indexOf(e) > -1 || used_scopes.length == 0; }))
                    continue;
                var original_value = style_binding.original_value, parsed_value = original_value, last_value = style_binding.last_value;
                for (var _c = 0, _d = style_binding.brackets; _c < _d.length; _c++) {
                    var bracket = _d[_c];
                    var _bracket = bracket.replace('{', '').replace('}', '');
                    eval("" + eval_scopes + ";\n                    _bracket = " + _bracket + ";\n                 ");
                    parsed_value = parsed_value.replace(bracket, _bracket);
                }
                var element = style_binding.element;
                if (style_binding.element.getAttribute('style') !== parsed_value) {
                    style_binding.element.setAttribute('style', parsed_value);
                    style_binding.last_value = parsed_value;
                }
            }
        };
        DataBinding.prototype.eval_visible_condition = function () {
            if (!this.visible_condition)
                return true;
            var value = true, scopes = this.scopes, eval_scopes = this.eval_scopes(), expression = this.visible_condition.expression;
            eval(eval_scopes + ";\n            value = " + expression + ";\n        ");
            value ? this.show() : this.hide();
            return value;
        };
        DataBinding.prototype.create_scope = function (name) {
            var _cons = declarations_1.Declarations.scopes.filter(function (e) { return e.prototype._karrot_name == name; });
            if (_cons.length == 0)
                throw new Error('No scope with name ' + name + '.');
            if (_cons.length > 1)
                throw new Error('Duplicate scopes with name ' + name + '.');
            var _con = _cons[0], scope_services = [], new_services = [], addons = [];
            if (_con.prototype.services) {
                var _loop_4 = function (service) {
                    var _service = this_1.services.filter(function (s) { return s._karrot_name == service.prototype._karrot_name; })[0];
                    if (!_service) {
                        _service = Reflect.construct(service, []);
                        this_1.services.push(_service);
                    }
                    scope_services.push(_service);
                };
                var this_1 = this;
                for (var _a = 0, _b = _con.prototype.services; _a < _b.length; _a++) {
                    var service = _b[_a];
                    _loop_4(service);
                }
            }
            if (_con.prototype.addons)
                for (var _c = 0, _d = _con.prototype.addons; _c < _d.length; _c++) {
                    var addon = _d[_c];
                    addons.push(Reflect.construct(addon, []));
                }
            var instance = Reflect.construct(_con, scope_services.concat(addons));
            instance._karrot_init(unique_value_1.UniqueValue.UniqueStorage, this);
            return instance;
        };
        DataBinding.prototype.create_dynamic_scope = function (name, owner_name, data) {
            return { _karrot_name: name, _karrot_id: owner_name, data: data, is_dynamic: true };
        };
        DataBinding.prototype.add_scope = function (scope) {
            this.scopes.push(scope);
        };
        DataBinding.prototype.create_child = function (element) {
            var child = new DataBinding(element, this.scopes.slice(), this.services.slice(), this.router);
            child.parent = this;
            this.children.push(child);
            return child;
        };
        DataBinding.prototype.create_loop_child = function (element, expression) {
            var child = new LoopDataBinding(element, this.scopes.slice(), this.services.slice(), expression, this.router);
            child.parent = this;
            this.children.push(child);
            return child;
        };
        /*create_route_child(element:Element) {
            let child = new RouteDataBinding(element, [...this.scopes], [...this.services],  this.router);
            child.parent = this;
            this.children.push(child);
            return child;
        }*/
        DataBinding.prototype.hide = function () {
            if (this.is_visible) {
                if (!this.position_element) {
                    this.position_element = document.createComment(' Karrot hidden element ');
                    this.data_element.parentNode.insertBefore(this.position_element, this.data_element);
                }
                this.is_visible = false;
                this.data_element.parentNode.removeChild(this.data_element);
            }
        };
        DataBinding.prototype.show = function () {
            if (!this.is_visible) {
                this.is_visible = true;
                this.position_element.parentNode
                    .insertBefore(this.data_element, this.position_element.nextSibling ?
                    this.position_element.nextSibling :
                    this.position_element);
                this.position_element.parentNode.removeChild(this.position_element);
                this.position_element = undefined;
            }
        };
        DataBinding.prototype.toogle = function () {
            this.is_visible ? this.hide() : this.show();
        };
        DataBinding.prototype.remove_local_scopes = function () {
            for (var _a = 0, _b = this.local_scopes; _a < _b.length; _a++) {
                var scope = _b[_a];
                if (scope.k_destroy)
                    scope.k_destroy();
                this.scopes.splice(this.scopes.indexOf(scope), 1);
            }
        };
        DataBinding.prototype.remove = function () {
            this.data_element.innerHTML = '';
            //delete this.parent.children.splice(this.parent.children.indexOf(this), 1);
            //delete this;
        };
        return DataBinding;
    }());
    exports.DataBinding = DataBinding;
    var LoopDataBinding = (function (_super) {
        __extends(LoopDataBinding, _super);
        function LoopDataBinding(data_element, scopes, services, expression, router) {
            if (router === void 0) { router = undefined; }
            var _this = _super.call(this, data_element, scopes, services, router) || this;
            _this.data_element = data_element;
            _this.scopes = scopes;
            _this.services = services;
            _this.expression = expression;
            _this.router = router;
            _this.pipes = [];
            _this.position_element = document.createComment(' Karrot loop ');
            _this.data_element.parentNode.insertBefore(_this.position_element, _this.data_element);
            _this.data_element.parentNode.removeChild(_this.data_element);
            return _this;
        }
        LoopDataBinding.prototype.init = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    this.get_array_owner();
                    this._parse();
                    return [2 /*return*/];
                });
            });
        };
        LoopDataBinding.prototype._parse = function () {
            var array_item = this.loop_eval();
            array_item = this.check_sort(array_item);
            for (var _i in array_item)
                this.add_loop_child(array_item[_i], _i);
        };
        LoopDataBinding.prototype.change_detect = function (used_scopes) {
            var _this = this;
            if (used_scopes === void 0) { used_scopes = []; }
            console.log('change detect array', this.children_used_scope, used_scopes);
            if (!this.children_used_scope.some(function (e) { return used_scopes.indexOf(e) > -1 || used_scopes.length == 0; }))
                return;
            console.log('change detect array2');
            var array_item = this.loop_eval(), children_to_remove = [], index = 0;
            array_item = this.check_sort(array_item);
            var _loop_5 = function (child) {
                var child_array = array_item.filter(function (e) {
                    return child.scopes.some(function (s) {
                        return s.data == e ? true : false;
                    });
                });
                if (child_array.length == 0)
                    children_to_remove.push(child);
            };
            //remove first rak
            for (var _a = 0, _b = this.children; _a < _b.length; _a++) {
                var child = _b[_a];
                _loop_5(child);
            }
            for (var _c = 0, children_to_remove_1 = children_to_remove; _c < children_to_remove_1.length; _c++) {
                var child = children_to_remove_1[_c];
                child.remove();
            }
            var _loop_6 = function (_i) {
                var child = this_2.children.filter(function (e) {
                    return e.scopes.some(function (s) {
                        return s.data == array_item[_i] ? true : false;
                    });
                });
                if (child.length == 0) {
                    console.log('add rak');
                    this_2.add_loop_child(array_item[_i], index);
                    index++;
                }
                else if (child[0].eval_visible_condition()) {
                    child[0].scopes.filter(function (e) { return e._karrot_name == _this.index_name; })[0].data = index;
                    if (child[0] != this_2.children[_i]) {
                        this_2._set_position_of_element(child[0], index);
                    }
                    index++;
                }
            };
            var this_2 = this;
            for (var _i in array_item) {
                _loop_6(_i);
            }
            for (var _d = 0, _e = this.children; _d < _e.length; _d++) {
                var child = _e[_d];
                child.change_detect(used_scopes);
            }
        };
        LoopDataBinding.prototype.check_sort = function (array) {
            var sort_pipe = this.pipes.filter(function (p) { return p.type == 'sort'; })[0];
            if (!sort_pipe)
                return array;
            var _sort_values = sort_pipe.expression.split('&&'), sort_values = [];
            for (var _a = 0, _sort_values_1 = _sort_values; _a < _sort_values_1.length; _a++) {
                var sort_value = _sort_values_1[_a];
                var sort_type = sort_value.indexOf('@asc') > -1 ? 'asc' : 'desc', sort_exp = sort_value.replace(/\s|@asc|@desc/gm, '');
                if (sort_exp.indexOf(this.fake_scope_name + '.') == 0)
                    sort_exp = sort_exp.replace(this.fake_scope_name + '.', '');
                sort_values.push({ exp: sort_exp, type: sort_type });
            }
            sort_values = sort_values.reverse();
            array.sort(function (a, b) {
                for (var i = 0; i < sort_values.length; i++) {
                    var f_val = sort_values[i], n_val = sort_values[i + 1], f_v_a = f_val.type == 'asc' ? a[f_val.exp] : b[f_val.exp], f_v_b = f_val.type == 'asc' ? b[f_val.exp] : a[f_val.exp];
                    if (!n_val || a[n_val.exp] == b[n_val.exp])
                        if (isNaN(f_v_a))
                            return f_v_a.localeCompare(f_v_b);
                        else
                            return f_v_a - f_v_b;
                }
            });
            return array;
        };
        LoopDataBinding.prototype.add_loop_child = function (item, index) {
            var copy_element = this.data_element.cloneNode(true);
            copy_element.removeAttribute('*k:for');
            var child = this.create_child(copy_element);
            child.scopes.push(child.create_dynamic_scope(this.fake_scope_name, this.owner_name, item));
            child.scopes.push(child.create_dynamic_scope(this.index_name, this.owner_name, index));
            for (var _a = 0, _b = this.pipes; _a < _b.length; _a++) {
                var pipe = _b[_a];
                if (pipe.type == 'filter')
                    child.visible_condition = new visible_condition_1.VisibleCondition(copy_element, child.scopes, pipe.expression);
            }
            this.set_position_of_element(copy_element, index);
            child.init();
        };
        LoopDataBinding.prototype.set_position_of_element = function (element, index) {
            var position_element = index > 0 && this.children[index - 1] ?
                this.children[index - 1].data_element.parentNode ? this.children[index - 1].data_element : this.children[index - 1].position_element :
                this.position_element;
            if (position_element.nextSibling)
                position_element.parentNode.insertBefore(element, position_element.nextSibling);
            else
                position_element.parentNode.appendChild(element);
        };
        LoopDataBinding.prototype._set_position_of_element = function (data_binding, index) {
            this.children.splice(this.children.indexOf(data_binding), 1);
            this.children.splice(index, 0, data_binding);
            this.set_position_of_element(data_binding.data_element, index);
        };
        LoopDataBinding.prototype.get_array_owner = function () {
            var sections = this.expression.split('|'), exp = sections.shift().split('of'), scope_name = exp[0].replace(/\s/, ''), scope_array = exp[1].replace(/\s/, ''), owner = scope_array.split('.')[0], owner_id = this.scopes.filter(function (el) { return el._karrot_name == owner; })[0]._karrot_id;
            for (var _a = 0, sections_1 = sections; _a < sections_1.length; _a++) {
                var pipe = sections_1[_a];
                var _pipe = pipe.split(':');
                this.pipes.push({ type: _pipe[0].replace(/\W/gm, ''), expression: _pipe[1].replace(/\s/, '') });
            }
            this.owner_name = owner_id;
            if (scope_name.indexOf(',') != 0) {
                this.index_name = scope_name.split(',')[1];
                scope_name = scope_name.split(',')[0];
            }
            this.fake_scope_name = scope_name;
            this.used_scopes = this.parent.used_scopes;
        };
        LoopDataBinding.prototype.loop_eval = function () {
            var sections = this.expression.split('|'), exp = sections.shift().split('of'), scope_name = exp[0].replace(/\s/, ''), scope_array = exp[1].replace(/\s/, ''), scope_data, scopes = this.scopes, eval_scopes = this.eval_scopes();
            eval(eval_scopes + "\n            scope_data = " + scope_array + ";\n        ");
            return scope_data;
        };
        return LoopDataBinding;
    }(DataBinding));
    exports.LoopDataBinding = LoopDataBinding;
});
/*
Tworzyć DataBinding

1. Stworzyć element, stworzyć DataBinding, dołączyć fake_scope
.*/
/*
export class RouteDataBinding extends DataBinding {
    constructor(public data_element:Element, public scopes:Array<IScope>, public services:Array<IService>,  public router:Router) {
        super(data_element, scopes, services)
    }

    init() {
        this.router.route_data_binding = this;
        this.create_route_scope();

        this._parse();
        this.hide_condition();
        this.eval_events();
        for(let child of this.children)
            child.init();

        this.eval_text_nodes();
    }

    change_route(_pathname) {
        console.log('change route');
        this.remove_local_scopes();
        this.data_element.innerHTML = '';
        this.create_route_scope(_pathname);
    }

    create_route_scope(_pathname = '') {
        console.log('create new route');
        let {route_object, params} = this.router.get_route(_pathname),
            _con = route_object.route,
            scope_services = [],
            new_services = [];
            

        for(let service of _con.prototype.services) {
            let _service = this.services.filter((s) => s._karrot_name == service.prototype._karrot_name)[0];
            if(!_service) {
                _service = Reflect.construct(service, []);
                this.services.push(_service);
            }

            scope_services.push(_service);
        }
        let instance = <IRoute>Reflect.construct(_con, [...params, ...scope_services]);
        instance._karrot_init();

        this.data_element.innerHTML = instance.template;
        this.add_scope(instance);
        return instance;
    }
}

*/ ;
define('karrot/core/router',["require", "exports"], function (require, exports) {
    "use strict";
    var Router = (function () {
        function Router(routers) {
            this.routers = routers;
            this.base_url = document.getElementsByTagName('base')[0].getAttribute('href');
        }
        Router.prototype.get_route = function (_pathname) {
            if (_pathname === void 0) { _pathname = ''; }
            if (this.current_route) {
            }
            var pathname = _pathname != '' ? _pathname : document.location.pathname.replace(this.base_url, ''), split_pathname = pathname.split('/');
            console.log('pathname: ', pathname);
            for (var _i = 0, _a = this.routers; _i < _a.length; _i++) {
                var route_object = _a[_i];
                var route_pathname = route_object.path, params = [];
                console.log(route_pathname);
                if (route_object.params)
                    for (var _b = 0, _c = route_object.params; _b < _c.length; _b++) {
                        var route_param = _c[_b];
                        route_pathname = route_pathname.replace(':' + route_param, '_karrot_param');
                    }
                var split_route_pathname = route_pathname.split('/'), i = 0;
                for (i = 0; i < split_pathname.length; i++) {
                    if (split_route_pathname[i] == '_karrot_param') {
                        params.push(split_pathname[i]);
                        continue;
                    }
                    if (!split_route_pathname[i])
                        break;
                    if (split_pathname[i] != split_route_pathname[i])
                        break;
                }
                console.log(split_pathname);
                if (i == split_pathname.length) {
                    console.log(split_pathname, 'pass');
                    if (route_object.redirect_to) {
                        console.log('redirect_from', split_pathname, route_object);
                        return this.get_route(route_object.redirect_to);
                    }
                    if (route_object.route) {
                        console.log('return ', route_object, params);
                        return { route_object: route_object, params: params };
                    }
                }
            }
        };
        Router.prototype.route_to = function (pathname) {
            history.pushState({}, "Karrot", this.base_url + pathname);
        };
        return Router;
    }());
    exports.Router = Router;
});
define('karrot/decorators/app.decorator',["require", "exports", "../core/data_binding", "../core/router", "../helpers/unique_value", "../core/declarations"], function (require, exports, data_binding_1, router_1, unique_value_1, declarations_1) {
    "use strict";
    function _App(options) {
        return function (constructor) {
            constructor.prototype._karrot_name = 'app';
            constructor.prototype.base_url = document.getElementsByTagName('base')[0].getAttribute('href');
            constructor.prototype.base_app_element = document.querySelector('*[__karrot_app]');
            constructor.prototype.__init_objects = function (all_objects) {
                //Call on_load function of all objects.
                for (var _i = 0, all_objects_1 = all_objects; _i < all_objects_1.length; _i++) {
                    var object = all_objects_1[_i];
                    if (this.__karrot_routing)
                        object.router = this.router;
                }
                //Call __karrot_init of all objects.
                for (var _a = 0, all_objects_2 = all_objects; _a < all_objects_2.length; _a++) {
                    var object = all_objects_2[_a];
                    if (object.__karrot_init != undefined)
                        object.__karrot_init();
                }
                //Check watching events of all objects.
                for (var _b = 0, all_objects_3 = all_objects; _b < all_objects_3.length; _b++) {
                    var object = all_objects_3[_b];
                    if (object.__check_is_watching != undefined) {
                        object.__check_is_watching();
                    }
                }
                for (var _c = 0, all_objects_4 = all_objects; _c < all_objects_4.length; _c++) {
                    var object = all_objects_4[_c];
                    if (object.__check_is_watched != undefined) {
                        object.__check_is_watched();
                    }
                }
                //Call on_load function of all objects.
                for (var _d = 0, all_objects_5 = all_objects; _d < all_objects_5.length; _d++) {
                    var object = all_objects_5[_d];
                    if (object.on_load != undefined)
                        object.on_load();
                }
            };
            constructor.prototype.__karrot_init = function () {
                var _this = this;
                declarations_1.Declarations.init(options.declarations);
                console.log('declarations', [declarations_1.Declarations]);
                if (options.routing) {
                    this.router = new router_1.Router(options.routing);
                }
                if (options.template) {
                    this.template = options.template;
                    this.__create_template();
                }
                else if (options.templateUrl) {
                    var promise = new Promise(function (resolve, reject) {
                        var req = new XMLHttpRequest();
                        req.open("GET", options.templateUrl, true);
                        req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                        req.onload = function () {
                            resolve(req.responseText);
                        };
                        req.send();
                    });
                    promise.then(function (res) {
                        _this.template = res;
                        _this.__create_template();
                    });
                }
            };
            constructor.prototype.create_services = function () {
                var services = [];
                for (var _i = 0, _a = options.services; _i < _a.length; _i++) {
                    var service_con = _a[_i];
                    var addons = [];
                    if (service_con.prototype.addons) {
                        for (var _b = 0, _c = service_con.prototype.addons; _b < _c.length; _b++) {
                            var addon = _c[_b];
                            addons.push(Reflect.construct(addon, []));
                        }
                    }
                    services.push(Reflect.construct(service_con, addons.slice()));
                }
                return services;
            };
            constructor.prototype.create_global_scopes = function (services) {
                var scopes = [];
                for (var _i = 0, _a = options.scopes; _i < _a.length; _i++) {
                    var scope_con = _a[_i];
                    var scope_services = [];
                    var _loop_1 = function (service) {
                        var _service = services.filter(function (s) { return s._karrot_name == service.prototype._karrot_name; })[0];
                        if (!_service) {
                            throw new Error('No service ' + service.prototype._karrot_name + ' in app decorator.');
                        }
                        scope_services.push(_service);
                    };
                    for (var _b = 0, _c = scope_con.prototype.services; _b < _c.length; _b++) {
                        var service = _c[_b];
                        _loop_1(service);
                    }
                    var instance = Reflect.construct(scope_con, [scope_services]);
                    scopes.push(instance);
                }
                return scopes;
            };
            constructor.prototype.__create_template = function () {
                var _this = this;
                this.template = this.template.replace(/\{\s+/gm, '{');
                this.template = this.template.replace(/\s+\}/gm, '}');
                this._clone = this.base_app_element.cloneNode(true);
                this._clone.innerHTML = this.template;
                var services = this.create_services(), scopes = this.create_global_scopes(services);
                this.data_binding = new data_binding_1.DataBinding(this._clone, scopes, services, this.router);
                for (var _i = 0, scopes_1 = scopes; _i < scopes_1.length; _i++) {
                    var scope = scopes_1[_i];
                    scope._karrot_init(unique_value_1.UniqueValue.UniqueStorage, this.data_binding);
                }
                var load_promise = new Promise(function (resolve, reject) {
                    console.log('loaded dom0', document.readyState);
                    if (document.readyState == 'complete') {
                        resolve();
                    }
                    document.onreadystatechange = function () {
                        if (document.readyState === "complete") {
                            resolve();
                        }
                    };
                });
                Promise.all([this.data_binding.init(), load_promise]).then(function () {
                    console.log('loaded');
                    _this.base_app_element.parentElement.replaceChild(_this._clone, _this.base_app_element);
                    //this.base_app_element.replaceWith(this._clone);
                    _this.base_app_element = _this._clone;
                    setTimeout(function () {
                        _this.base_app_element.classList.add('k-loaded');
                        if (_this.init) {
                            _this.init();
                        }
                    }, 1);
                });
            };
        };
    }
    exports._App = _App;
});
define('karrot/decorators/service.decorator',["require", "exports"], function (require, exports) {
    "use strict";
    function _Service(options) {
        return function (constructor) {
            constructor.prototype._karrot_name = options.name;
            constructor.prototype.addons = options.addons;
            constructor.prototype._karrot_init = function () {
            };
        };
    }
    exports._Service = _Service;
});
define('karrot/decorators/addon.decorator',["require", "exports"], function (require, exports) {
    "use strict";
    function _Addon(options) {
        return function (constructor) {
            constructor.prototype.name = options.name;
        };
    }
    exports._Addon = _Addon;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define('karrot/defined/http.addon',["require", "exports", "../decorators/addon.decorator"], function (require, exports, addon_decorator_1) {
    "use strict";
    var Http = (function () {
        function Http() {
        }
        Http.prototype.get = function (url, params) {
            if (params === void 0) { params = {}; }
            var params_string = '?';
            for (var _param in params) {
                params_string += _param + '=' + params[_param];
                params_string += '&';
            }
            params_string = params_string.substring(0, params_string.length - 1);
            console.log(params_string);
            var promise = new Promise(function (resolve, reject) {
                var req = new XMLHttpRequest();
                req.open("GET", url, true);
                req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                req.onload = function () {
                    resolve(req);
                };
                req.send(params_string);
            });
            return promise;
        };
        Http.prototype.post = function (url, params) {
            if (params === void 0) { params = {}; }
            var params_string = '';
            for (var _param in params) {
                params_string += _param + '=' + params[_param];
                params_string += '&';
            }
            params_string = params_string.substring(0, params_string.length - 1);
            var promise = new Promise(function (resolve, reject) {
                var req = new XMLHttpRequest();
                req.open("POST", url, true);
                req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                req.onload = function () {
                    if (req.status === 200)
                        resolve(req.response);
                    else
                        reject(new Error(req.statusText));
                };
                req.send(params_string);
            });
            return promise;
        };
        return Http;
    }());
    Http = __decorate([
        addon_decorator_1._Addon({
            name: 'http',
            allow_services: true,
            allow_scopes: true,
            allow_app: true,
        })
    ], Http);
    exports.Http = Http;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
define('services/tasks.service',["require", "exports", "../karrot/decorators/service.decorator", "../karrot/defined/http.addon"], function (require, exports, service_decorator_1, http_addon_1) {
    "use strict";
    var TasksService = (function () {
        function TasksService(http) {
            this.http = http;
            this.tasks = [];
            console.log('???', http);
        }
        TasksService.prototype.load_data = function () {
            return __awaiter(this, void 0, void 0, function () {
                var response, tasks;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.http.get('http://api.openweathermap.org/data/2.5/forecast/city', { q: 'warsaw', APPID: '48cff6357482b656af9be28f2e8fd9e4' })];
                        case 1:
                            response = _a.sent(), tasks = '';
                            console.log('??', response);
                            return [2 /*return*/];
                    }
                });
            });
        };
        return TasksService;
    }());
    TasksService = __decorate([
        service_decorator_1._Service({
            name: 'tasks_service',
            addons: [http_addon_1.Http]
        })
    ], TasksService);
    exports.TasksService = TasksService;
    function delay(time) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve();
            }, time);
        });
    }
    exports.delay = delay;
});
define('karrot/core/change_detector',["require", "exports"], function (require, exports) {
    "use strict";
    var ChangeDetector = (function () {
        function ChangeDetector(data_manager) {
            this.data_manager = data_manager;
        }
        return ChangeDetector;
    }());
    exports.ChangeDetector = ChangeDetector;
});
/*
change_detector
Każdy obiekt posiada change_detector, informuje on data_manager by
sprawdził zmiany w danych scopes.

nodes_manager
Posiada wszystkie nodes, które przekazuje data_manager, parsuje nodes,
dodając odpowiednie przedraki (this.storages.user_storage) etc.

data_manager
Posiada wszystkie obiekty posiadające dane, oraz nodes_manager.
Updejtuje text_nodes.

user_storage.change_detector -> data_manager
data_manager.nodes_manager -> nodes
data_manager -> storages, nodes_manager

??
Każdy change_detector posiada data_manager, który zawiera sporo danych.
(optymalizacja?)


Tam gdzie tworze handlebars, potrzebuję obiektów z danymi.
   - NodesManager posiadający wszystkie obiekty i nodesy.
   {
       node: jakiś_tam_element_który_to_trzyma,
       need_update: true,
       binds: [
           {user_storage.username}
       ],
       last_value: ''
   }

   parse_node(element, scope) {
       //scope = {
           'user_storage': 'this.scopes["user_storage"]',
           'custom': 'scopes["user_storage"].custom_array[1]
       }
   }
   
   1. Sprawdzam zmianę porównując nowy tekst do 'last_value'.
   2. Każdy change detector posiada instancję NodesManager
   3. ChangeDetector "zleca" sprawdzenie nodesów tylko powiązanych cośków.//todo, scopes w ChangeDetector?.

   problemy
   Jeśli chcę tworzyć eventy w nodes_manager, muszę mieć dostęp do change_detectorów. (change_detector mają nodes_manager, nodes_manager change_detector)

   rozwiązania


*/ ;
define('karrot/decorators/scope.decorator',["require", "exports", "../core/change_detector"], function (require, exports, change_detector_1) {
    "use strict";
    function _Scope(options) {
        return function (constructor) {
            constructor.prototype._karrot_name = options.name;
            constructor.prototype.services = options.services;
            constructor.prototype.addons = options.addons;
            console.log(constructor.prototype.services);
            constructor.prototype._karrot_init = function (_id, data_binding) {
                this.change_detector = new change_detector_1.ChangeDetector(data_binding);
                this._karrot_id = _id;
            };
        };
    }
    exports._Scope = _Scope;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define('karrot/defined/cookies.addon',["require", "exports", "../decorators/addon.decorator"], function (require, exports, addon_decorator_1) {
    "use strict";
    var Cookies = (function () {
        function Cookies() {
        }
        Cookies.prototype.set = function (cookie_name, cookie_value, expire_days) {
            var d = new Date();
            d.setTime(d.getTime() + (expire_days * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cookie_name + "=" + cookie_value + ";" + expires + ";path=/";
        };
        Cookies.prototype.get = function (cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        };
        return Cookies;
    }());
    Cookies = __decorate([
        addon_decorator_1._Addon({
            name: 'cookies',
            allow_services: true,
            allow_scopes: true,
            allow_app: true,
        })
    ], Cookies);
    exports.Cookies = Cookies;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
define('scopes/weather.scope',["require", "exports", "../karrot/decorators/scope.decorator", "../karrot/defined/http.addon", "../karrot/defined/cookies.addon"], function (require, exports, scope_decorator_1, http_addon_1, cookies_addon_1) {
    "use strict";
    var WeatherScope = (function () {
        function WeatherScope(http, cookies) {
            this.http = http;
            this.cookies = cookies;
            this.cities = [];
            this.search_city = '';
        }
        WeatherScope.prototype.load_data = function () {
            return __awaiter(this, void 0, void 0, function () {
                var cookies_info, _i, cookies_info_1, cookie;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            cookies_info = this.cookies.get('cities').split(',');
                            _i = 0, cookies_info_1 = cookies_info;
                            _a.label = 1;
                        case 1:
                            if (!(_i < cookies_info_1.length))
                                return [3 /*break*/, 4];
                            cookie = cookies_info_1[_i];
                            if (!(cookie != ''))
                                return [3 /*break*/, 3];
                            return [4 /*yield*/, this.add_city(cookie)];
                        case 2:
                            _a.sent();
                            _a.label = 3;
                        case 3:
                            _i++;
                            return [3 /*break*/, 1];
                        case 4: return [2 /*return*/];
                    }
                });
            });
        };
        WeatherScope.prototype.get_city = function (e) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.add_city(this.search_city)];
                        case 1:
                            _a.sent();
                            this.save_cookies();
                            this.search_city = '';
                            return [2 /*return*/];
                    }
                });
            });
        };
        WeatherScope.prototype.add_city = function (name) {
            return __awaiter(this, void 0, void 0, function () {
                var response, weather, city, _i, _a, _city;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            console.log('name', name);
                            return [4 /*yield*/, this.http.get('http://api.openweathermap.org/data/2.5/forecast/daily?q=' + name + '&APPID=48cff6357482b656af9be28f2e8fd9e4&units=metric')];
                        case 1:
                            response = _b.sent(), weather = JSON.parse(response.responseText);
                            console.log(weather);
                            city = new City(weather);
                            for (_i = 0, _a = this.cities; _i < _a.length; _i++) {
                                _city = _a[_i];
                                if (_city.id == city.id) {
                                    this.cities.splice(this.cities.indexOf(_city), 1);
                                }
                            }
                            this.cities.unshift(city);
                            return [2 /*return*/];
                    }
                });
            });
        };
        WeatherScope.prototype.remove_city = function (index) {
            this.cities.splice(index, 1);
            this.save_cookies();
        };
        WeatherScope.prototype.save_cookies = function () {
            var cities_string = '';
            for (var _i = 0, _a = this.cities; _i < _a.length; _i++) {
                var city = _a[_i];
                cities_string += city.name + ',';
            }
            this.cookies.set('cities', cities_string, 30);
        };
        return WeatherScope;
    }());
    WeatherScope = __decorate([
        scope_decorator_1._Scope({
            name: 'weather',
            addons: [http_addon_1.Http, cookies_addon_1.Cookies]
        })
    ], WeatherScope);
    exports.WeatherScope = WeatherScope;
    var City = (function () {
        function City(api) {
            this.days = [];
            this.country = api.city.country;
            this.name = api.city.name;
            this.id = api.city.id;
            for (var api_day in api.list) {
                if (api_day <= 4)
                    this.days.push(new Day(api.list[api_day]));
            }
            this.currentDay = this.days[0];
        }
        City.prototype.change_day = function (i) {
            this.currentDay = this.days[i];
        };
        return City;
    }());
    var Day = (function () {
        function Day(api) {
            this.day_temp = parseInt(api.temp.day);
            this.night_temp = parseInt(api.temp.night);
            this.humidity = api.humidity;
            this.pressure = api.pressure;
            this.clouds = api.clouds;
            this.wind_speed = parseInt(api.speed);
            this.time = new Date(api.dt * 1000);
            switch (api.weather[0].main) {
                case 'Clear':
                    this.weather_type = 'słonecznie';
                    this.weather_img = 'sunny';
                    break;
                case 'Snow':
                case 'Clouds':
                    this.weather_type = "pochmurnie";
                    this.weather_img = 'clouds';
                    break;
                case 'Rain':
                    this.weather_type = "deszczowo";
                    this.weather_img = 'rain';
                    break;
                default:
                    this.weather_type = api.weather[0].main;
                    this.weather_img = 'sunny';
            }
        }
        Day.prototype.getDate = function () {
            return this.time.getDate() + '.' + (this.time.getMonth() + 1) + '.' + this.time.getFullYear();
        };
        Day.prototype.getDayOfWeek = function () {
            var days = ['niedziela', 'poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota'];
            return days[this.time.getDay()];
        };
        return Day;
    }());
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define('karrot/defined/uppercase.pipe',["require", "exports"], function (require, exports) {
    "use strict";
    function _Pipe(data) {
        return function (constructor) {
            constructor.prototype._karrot_name = data.name;
        };
    }
    var Uppercase = (function () {
        function Uppercase() {
        }
        Uppercase.transform = function (expression, params) {
            console.log('params', params);
            return expression.toUpperCase();
        };
        return Uppercase;
    }());
    Uppercase = __decorate([
        _Pipe({
            name: 'uppercase'
        })
    ], Uppercase);
    exports.Uppercase = Uppercase;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define('app',["require", "exports", "./karrot/core/init", "./karrot/decorators/app.decorator", "./services/tasks.service", "./scopes/weather.scope", "./karrot/defined/uppercase.pipe"], function (require, exports, init_1, app_decorator_1, tasks_service_1, weather_scope_1, uppercase_pipe_1) {
    "use strict";
    var App = (function () {
        function App() {
            console.log('instancja app');
        }
        App.prototype.init = function () {
            console.log('app init');
        };
        return App;
    }());
    App = __decorate([
        app_decorator_1._App({
            templateUrl: 'templates/main.html',
            services: [tasks_service_1.TasksService],
            global_scopes: [],
            declarations: {
                scopes: [weather_scope_1.WeatherScope],
                pipes: [uppercase_pipe_1.Uppercase]
            },
            scopes: [] /*,
            //Deklarować mogę: scopes, pipes,
            routing: [
                {
                   path: '',
                   redirect_to: 'rak/a'
                },
                {
                    path: 'rak/a',
                    route: MainRoute
                },
                {
                    path: 'tasks',
                    route: TasksRoute
                }
            ]*/
        })
    ], App);
    exports.App = App;
    init_1.KarrotInit(App);
});
/*

*/ ;
