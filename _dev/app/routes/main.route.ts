import {_Route} from '../karrot/decorators/route.decorator';

@_Route({
    name: 'main',
    template: `
        <div>
            {main.test}
            Routing kek
        </div>
    `,
    services: []
})
export class MainRoute {
    test:string = 'test??';
    constructor() {
        console.log('main route', this)
    }
}