import {_Route} from '../karrot/decorators/route.decorator';

@_Route({
    name: 'tasks',
    template: `
        <ul>
            <li *k:for="a, i of test_scope.tasks | 
                filter: a.name.indexOf(test_scope.search) >= 0 |
                sort: a.done @asc && a.priority @desc"
                __karrot_scope_local_scope>
                <p>{a.name} </p>
                <p>priority: {a.priority}</p>
                <p>Done: {a.done}</p>
                <p k:click="local_scope.change()">{local_scope.rak2}</p>
            </li>
        </ul>
    `,
    services: []
})
export class TasksRoute {
    test:string = 'test??';
    constructor() {
        console.log('main route', this)
    }
}