import {IRoute} from '../interfaces/route.interface';

export class Router {
    route_data_binding;
    current_route:IRoute;
    base_url:string;
    constructor(public routers:Array<any>){
        this.base_url = document.getElementsByTagName('base')[0].getAttribute('href');
    }
    get_route(_pathname = '') {
        if(this.current_route) {
            //delete
        }
        let pathname = _pathname != '' ? _pathname : document.location.pathname.replace(this.base_url, ''),
            split_pathname = pathname.split('/');

        console.log('pathname: ', pathname);
        for(let route_object of this.routers) {
            let route_pathname = route_object.path,
                params = [];
            console.log(route_pathname);
            if(route_object.params) 
                for(let route_param of route_object.params) {
                    route_pathname = route_pathname.replace(':'+route_param, '_karrot_param')
                }

            let split_route_pathname = route_pathname.split('/'),
                i = 0;

            for( i = 0; i<split_pathname.length;i++) {

                if(split_route_pathname[i] == '_karrot_param') {
                    params.push(split_pathname[i]);
                    continue;
                }
                        
                if(!split_route_pathname[i])
                    break;

                if(split_pathname[i] != split_route_pathname[i])
                    break;
            }
            console.log(split_pathname)
            if(i == split_pathname.length) {    
                console.log(split_pathname, 'pass')
                if(route_object.redirect_to){
                    console.log('redirect_from', split_pathname, route_object);
                    return this.get_route(route_object.redirect_to)
                }

                if(route_object.route) {
                    console.log('return ', route_object, params)
                    return {route_object, params};
                }
            }
        } 

    }

    route_to(pathname) {
        history.pushState({}, "Karrot", this.base_url+pathname);
        
    }
}