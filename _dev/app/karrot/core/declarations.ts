export class Declarations {
    static scopes:Array<any> = [];
    static pipes:Array<any> = [];

    static init(declarations:any) {
        this.scopes = declarations.scopes ? declarations.scopes : 0;
        this.pipes = declarations.pipes ? declarations.pipes : 0;
        
    }
}