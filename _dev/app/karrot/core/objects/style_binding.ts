import {IScope} from '../../interfaces/scope.interface';

import {BindingObject} from './_binding_object';

import {BracketsHelper} from '../../helpers/brackets_helper';


export class StyleBinding extends BindingObject {
    public brackets:Array<string> = [];
    public original_value:string;
    public last_value:string;
    
    constructor(public element:Element, scopes:Array<IScope>, public style:string) {
        super(element, scopes);
        let brackets = BracketsHelper._brackets(style);
        this.original_value = style;
        
        for(let bracket of brackets) { 
            this.brackets.push(bracket);
            this._check(bracket, scopes);
        }
    }

   
    
}