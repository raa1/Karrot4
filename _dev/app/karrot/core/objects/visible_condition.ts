import {IScope} from '../../interfaces/scope.interface';

import {BindingObject} from './_binding_object';

import {BracketsHelper} from '../../helpers/brackets_helper';


export class VisibleCondition extends BindingObject {
    public last_value:string;

    constructor(public element:Element, scopes:Array<IScope>, public expression:string) {
        super(element, scopes)
        this._check(expression, scopes)
    }

}