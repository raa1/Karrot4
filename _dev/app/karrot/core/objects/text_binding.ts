import {IScope} from '../../interfaces/scope.interface';

import {BindingObject} from './_binding_object';

import {BracketsHelper} from '../../helpers/brackets_helper';


export class TextBinding extends BindingObject {
    public brackets:Array<Bracket> = [];
    public original_value:string;
    
    constructor(public node:Node, scopes:Array<IScope>) {
        super(<Element>node, scopes);
        this.original_value = node.nodeValue;
        
        let brackets = BracketsHelper._brackets(this.node.nodeValue);

        for(let bracket of brackets) { 
            let {expression, pipes} = this._check(bracket, scopes);
            this.brackets.push(new Bracket(expression, pipes, bracket));
        }
    }

   
    
}
class Bracket {
    constructor(public expression:string, public pipes:any, public original_value) {

    }
}

//pipe
//params