import {IScope} from '../../interfaces/scope.interface';

import {BracketsHelper} from '../../helpers/brackets_helper';
import {Declarations} from '../declarations';

export class BindingObject {
    public scopes:Array<string> = [];
    public pipes:{[name: string] : any} = {};
    constructor(public element:Element, scopes:Array<IScope>) {

    }

    _check(_expression:string, scopes:Array<IScope>) {
        let expression = _expression.replace('{', '').replace('}', ''),
            pipes_expressions = [],
            pipes = {};

        let split_exp = expression.split('|');
        if(split_exp) {
            expression = split_exp.shift();
            pipes_expressions = split_exp;
        }
        
        let  text_string= BracketsHelper._replace_quotes(expression),
            _values = BracketsHelper._values(text_string);
                
        for(let _pipe of pipes_expressions) {
            let __pipe = _pipe,
                __params:string = '';

            let split_pipe:Array<string> = _pipe.split(':');
            if(split_pipe) {
                __pipe = split_pipe.shift().replace(/\s/, '');
                __params = split_pipe[0];
            }
                
            let pipe = Declarations.pipes.filter(e => e.prototype._karrot_name == __pipe)[0];
            if(pipe) {
                pipes[__pipe] = {pipe: pipe, params: __params};
            }
            console.log('pipes', pipes, __pipe, Declarations.pipes);
        }
        
        for(let _value of _values) {
            if(_value == '' || !isNaN(_value) || _value.indexOf('_karrot_text') == 0 || _value == 'e' || _value == 'event')
                continue;

            let scope = _value.split('.')[0],
                valid_scopes = scopes.filter((item)=> {
                    return item._karrot_name == scope ? true : false;
                });
                    
            if(valid_scopes.length == 0) 
                throw new Error('Invalid value "'+_value+'" in element:'+ this.element);
                    
            let id = valid_scopes[0]._karrot_id;
            if(this.scopes.indexOf(id) == -1) 
                this.scopes.push(id);
        }

        return {expression, pipes};
    }
}