import {IScope} from '../../interfaces/scope.interface';
import {BindingObject} from './_binding_object';
import {BracketsHelper} from '../../helpers/brackets_helper';

export class AttributeConditionBinding extends BindingObject {
    public attribute:string;
    public value:string;
    public false_value:string;
    public last_value:boolean;
    constructor(public element:Element, scopes:Array<IScope>, attr_name:string, public expression:string) {
        super(element, scopes)

        let _attr_name = attr_name.replace(/\(|\)/gm, '').split('.');
        this.attribute = _attr_name[0];
        if(_attr_name[1].indexOf('|') > 0) {
            let _value = _attr_name[1].split('|');
            this.value = _value[0];
            this.false_value = _value[1];
        } else {
            this.value = _attr_name[1];
        }

        this._check(expression, scopes);
        console.log('dis', this);
    }

}