import {IScope} from '../../interfaces/scope.interface';

import {BindingObject} from './_binding_object';

import {BracketsHelper} from '../../helpers/brackets_helper';


export class AttributeBinding extends BindingObject {
    public attribute:string;
    public last_value:string = '';

    constructor(public element:Element, scopes:Array<IScope>, attr_name:string, public expression:string) {
        super(element, scopes)

        let _attr_name = attr_name.replace(/\[|\]/gm, '');
        this.attribute = _attr_name;

        this._check(expression, scopes)
    }

}