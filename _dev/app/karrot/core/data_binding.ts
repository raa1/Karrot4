import {IService} from '../interfaces/service.interface';
import {IScope} from '../interfaces/scope.interface';
import {IRoute} from '../interfaces/route.interface';

import {BindingObject} from './objects/_binding_object';
import {AttributeBinding} from './objects/attribute_binding';
import {AttributeConditionBinding} from './objects/attribute_condition_binding';
import {StyleBinding} from './objects/style_binding';
import {EventBinding} from './objects/event_binding';
import {TextBinding} from './objects/text_binding';
import {VisibleCondition} from './objects/visible_condition';

import {UniqueValue} from '../helpers/unique_value';

import {BracketsHelper} from '../helpers/brackets_helper';

import {Declarations} from './declarations';

import {Router} from '../core/router';

/**
 * element: asd
 * scopes: []
 * brackets;
 * 
 */
export class DataBinding {

    public parent:DataBinding;
    public children:Array<DataBinding> = [];

    public text_bindings:Array<TextBinding> = [];
    public event_bindings:Array<EventBinding> = [];
    public attribute_bindings:Array<AttributeBinding> = [];
    public attribute_con_bindings:Array<AttributeConditionBinding> = [];
    public style_bindings:Array<StyleBinding> = [];

    public used_scopes:Array<string> = [];

    public is_visible:boolean = true;
    public position_element:Node;

    public visible_condition:VisibleCondition;

    constructor(public data_element:Element,
                public scopes:Array<IScope>,
                public services:Array<IService>,
                public router:Router = undefined) {}

    get main_data_binding():DataBinding { return !this.parent ? this : this.parent.main_data_binding; }
    get local_scopes():Array<IScope> { return this.scopes.filter((s) => this.parent ? this.parent.scopes.indexOf(s) == -1 ? true : false : true) ; }
    get dynamic_scopes():Array<IScope> { return this.scopes.filter((s) => s.is_dynamic ? true : false); }
    get local_dynamic_scopes():Array<IScope> { return this.local_scopes.filter((s) => s.is_dynamic ? true : false); }
    get binding_objects():Array<BindingObject> {return [...this.attribute_bindings,...this.attribute_con_bindings,...this.event_bindings,...this.text_bindings,...this.style_bindings];}
    get children_used_scope():Array<string> {
        let used_scopes = this.used_scopes;
        for(let child of this.children) {
  
            if(child.is_visible)
                used_scopes = used_scopes.concat(child.children_used_scope.filter(e => used_scopes.indexOf(e) == -1));
        }
        return used_scopes;
    }
    get local_services():Array<IService> {return this.parent ? this.services.filter((s) => this.parent.services.indexOf(s) == -1 ? true : false) : this.services;}

    init():Promise<any> {
        let promise = new Promise((resolve, reject) => {
            let children_promise = [];
            this._parse();
            this.parse_used_scopes();

            this._eval_events();

            this.init_services().then(() => {
                

                this._eval_texts();
                this._eval_attrs();
                this._eval_con_attrs();
                this._eval_styles();

                this.eval_visible_condition();

                for(let child of this.children) 
                    children_promise.push(child.init());

                Promise.all(children_promise).then(() => {
                    resolve();
                })
            })
            
            
        })
        
        return promise;
    }
    init_services():Promise<any> {
        let promise = new Promise((resolve, reject) => {
            let services_load = [];
            for(let service of this.local_services) {
                if(service.load_data) {
                    let load_data = service.load_data();
                    load_data.then(() => {
                        if(service.init)
                            service.init();
                    })
                    services_load.push(load_data);
                } else if(service.init) {
                    service.init();
                }
                
            }
            for(let scope of this.local_scopes) {
                if(scope.load_data) {
                    let load_data = scope.load_data();
                    load_data.then(() => {
                        if(scope.init)
                            scope.init();
                    })
                    services_load.push(load_data);
                } else if(scope.init) {
                    scope.init();
                }
            }
            Promise.all(services_load).then(() => {
                resolve();
            });
        })

        return promise;
    }
    //parse block
    _parse() {
        this._parse_nodes(this.data_element);
        
    }
    //Loop method, parse all nodes, starts with data_element.
    _parse_nodes(node:Node) {
        //Check is #text node, then create TextNode
        if(node.nodeName == '#text') {
            //if empty/no brackets, skip
            if(!node.nodeValue.match(/\S/) || 
               !BracketsHelper._brackets(node.nodeValue))
                return;

            let text_binding = new TextBinding(node, this.scopes);      
            this.text_bindings.push(text_binding);
            return;
        }

        //if not text node, convert to element
        let element = <Element>node,
            attr_bindings:Array<Attr> = [],
            attr_con_bindings:Array<Attr> = [],
            attr_k:Array<Attr> = [],
            is_data_element = this.data_element == element;

        /*if(element.nodeName == 'K-ROUTE' && this.data_element != element) {
            this.create_route_child(element);
            return;
        }*/

        //check events/directives/new scopes in attr
        for(let i = 0; i < element.attributes.length; i++) {
            let attr = element.attributes[i];
            if(!is_data_element) {
                if(attr.name.indexOf('*k:') == 0) {
                    switch(attr.name) {
                        case '*k:for':
                            this.create_loop_child(element, attr.value);
                            break;
                        case '*k:if':
                            let child = this.create_child(element);
                            child.visible_condition = new VisibleCondition(element, child.scopes, attr.value);
                            break;
                    }
                    return;
                }

                if(attr.name.indexOf('__karrot_scope_') == 0) {
                    let child = this.create_child(element),
                        scope_name = attr.name.replace('__karrot_scope_', ''),
                        instance = child.create_scope(scope_name);
                    child.add_scope(instance);
                    return;
                }
            }

            if(attr.name.indexOf('k:') == 0) attr_k.push(attr);
            if(attr.name.indexOf('[') == 0) attr_bindings.push(attr);
            if(attr.name.indexOf('(') == 0) attr_con_bindings.push(attr);
        }
        

        //check attributes bindings
        for(let k of attr_bindings) {
            element.removeAttribute(k.name);
            let attribute_binding = new AttributeBinding(element, this.scopes, k.name, k.value);

            if(k.name == '[value]') {
                let event = new EventBinding(element, this.scopes, 'k:keyup', k.value+'=e.target.value');
                this.event_bindings.push(event);
            }

            this.attribute_bindings.push(attribute_binding);
        }

        //check attributes condition bindings
        for(let k of attr_con_bindings) {
            element.removeAttribute(k.name);
            let attribute_con_binding = new AttributeConditionBinding(element, this.scopes, k.name, k.value);

            this.attribute_con_bindings.push(attribute_con_binding);
        }

        //check 'k:' attributes
        for(let k of attr_k) {
            element.removeAttribute(k.name);
            switch(k.name) {
                case 'k:style':
                    let style_binding = new StyleBinding(element, this.scopes, k.value);
                    this.style_bindings.push(style_binding);
                    break;
                default:
                    let event = new EventBinding(element, this.scopes, k.name, k.value);
                    this.event_bindings.push(event);
                    break;
            }
        }

            /*if(attr.name.indexOf('router-link') == 0) {
                let href = attr.value;
                element.removeAttribute('router-link');
                element.setAttribute('href', href);
                window.onpopstate = (event) => {
                    let _element = this.router.route_data_binding.data_element;
                    this.router.route_data_binding.remove();
                    let child = this.create_route_child(_element);
                    child.init();
                };
                element.addEventListener('click', (e) => {
                    e.preventDefault();
                    this.router.route_to(href);
                    let _element = this.router.route_data_binding.data_element;
                    this.router.route_data_binding.remove();
                    let child = this.create_route_child(_element);
                    child.init();
                })
            }*/
                      
        for(let childNode of element.childNodes) {
            this._parse_nodes(childNode);
        }
    }

    parse_used_scopes() {
        let used_scopes:Array<string> = [];
        for(let binding_object of this.binding_objects)
        {
            used_scopes = used_scopes.concat(binding_object.scopes.filter(e => {return used_scopes.indexOf(e) == -1}));
        }
        console.log(this, used_scopes, this.binding_objects);
        this.used_scopes = used_scopes;
    }
    change_detect(used_scopes:Array<string> = []) {
        if(!this.children_used_scope.some(e => used_scopes.indexOf(e) > -1 || used_scopes.length == 0))
            return;

        this.eval_visible_condition();
        if(!this.is_visible)
            return;

        /*this.check_attribute_handlers();
        this.eval_styles();
        this.eval_text_nodes();*/
        this._eval_texts();
        this._eval_attrs();
        this._eval_con_attrs();
        this._eval_styles();

        for(let child of this.children) {
            child.change_detect(used_scopes);
        }
    }

    eval_scopes():string {
        let eval_scope = '';
        for(let scope in this.scopes) {
            eval_scope += 'var '+ this.scopes[scope]._karrot_name + ' = scopes[' + scope + ']'+(this.scopes[scope].is_dynamic ? '.data' : '')+';\n';
        }

        return eval_scope;
    }

    _eval(expression:string) {
        let value = '',
            scopes = this.scopes;
            
        eval(this.eval_scopes()+`;
            value = `+expression+`;
        `);
        return value;
    }
 //uppercase.transform(10);
 //Declarations
    _eval_texts(used_scopes:Array<string> = []) {
        let scopes = this.scopes,
            eval_scopes = this.eval_scopes();
        for(let text_binding of this.text_bindings) {
            if(!text_binding.scopes.some(e => used_scopes.indexOf(e) > -1 || used_scopes.length == 0))
                continue;

            let original_value = text_binding.original_value,
                parsed_value = original_value;
            for(let bracket of text_binding.brackets) {
                let _bracket = bracket.expression,
                    pipes = bracket.pipes;
                eval(``+eval_scopes+`;
                    _bracket = `+_bracket+`;
                 `);
                 /*for(let pipe in bracket.pipes) {
                     console.log(pipe);
                     eval(``+eval_scopes+`;
                        _bracket = pipes['`+pipe+`'].pipe.transform(_bracket, `+pipes[pipe].params+`);
                    `);
                 }*/
                 parsed_value = parsed_value.replace(bracket.original_value, _bracket);
            }
            if(text_binding.node.nodeValue != parsed_value)
                text_binding.node.nodeValue = parsed_value;
        }
    }

    _eval_events(used_scopes:Array<string> = []) {
        let scopes = this.scopes,
            eval_scopes = this.eval_scopes();
        for(let event of this.event_bindings) {
            let evaled_event;
            event.element[event.attribute] = (e) => {
                eval(eval_scopes+';var event = e;\nevaled_event='+event.expression);

                switch(typeof evaled_event) {
                    case 'object':
                        if(evaled_event.then) {
                            evaled_event.then((response) => {
                                if(response !== false) {
                                    this.main_data_binding.change_detect(event.scopes);
                                }
                            });
                            break;
                        }
                    case 'boolean':
                        if(evaled_event === false)
                            break;
                    default:
                        this.main_data_binding.change_detect(event.scopes);
                }
                console.log(evaled_event);
                
            }
        }
    }

    _eval_attrs(used_scopes:Array<string> = []) {
        let scopes = this.scopes,
            eval_scopes = this.eval_scopes();
        for(let attr_binding of this.attribute_bindings) {
            if(!attr_binding.scopes.some(e => used_scopes.indexOf(e) > -1 || used_scopes.length == 0))
                continue;

            let value = '';
            eval(eval_scopes+`;
                value = `+attr_binding.expression+`;
            `);
            if(attr_binding.last_value != value) {
                switch(attr_binding.attribute) {
                    case 'class':
                        attr_binding.element.classList.remove(attr_binding.last_value);
                        attr_binding.element.classList.add(value)
                        break;
                    default:
                        attr_binding.element.setAttribute(attr_binding.attribute, value);
                        break;
                }
                attr_binding.last_value = value;
            }
        }
    }
    _eval_con_attrs(used_scopes:Array<string> = []) {
        let scopes = this.scopes,
            eval_scopes = this.eval_scopes();
        for(let attr_con_binding of this.attribute_con_bindings) {
            if(!attr_con_binding.scopes.some(e => used_scopes.indexOf(e) > -1 || used_scopes.length == 0))
                continue;

            let value:boolean;
            eval(eval_scopes+`;
                value = `+attr_con_binding.expression+`;
            `);
            if(attr_con_binding.last_value != value) {
                if(value) {
                    switch(attr_con_binding.attribute) {
                        case 'class':
                            attr_con_binding.element.classList.remove(attr_con_binding.false_value);
                            attr_con_binding.element.classList.add(attr_con_binding.value);
                            break;
                        default:
                            attr_con_binding.element.setAttribute(attr_con_binding.attribute, attr_con_binding.value);
                            break;
                    }
                } else {
                    switch(attr_con_binding.attribute) {
                        case 'class':
                            attr_con_binding.element.classList.remove(attr_con_binding.value);
                            attr_con_binding.element.classList.add(attr_con_binding.false_value);
                            console.log('add false value');
                            break;
                        default:
                            attr_con_binding.element.setAttribute(attr_con_binding.attribute, attr_con_binding.false_value);
                            break;
                    }
                }
                attr_con_binding.last_value = value;
            }
        }
    }

    _eval_styles(used_scopes:Array<string> = []) {
        let scopes = this.scopes,
            eval_scopes = this.eval_scopes();

        for(let style_binding of this.style_bindings) {
            if(!style_binding.scopes.some(e => used_scopes.indexOf(e) > -1 || used_scopes.length == 0))
                continue;

            let original_value = style_binding.original_value,
                parsed_value = original_value,
                last_value = style_binding.last_value;

            for(let bracket of style_binding.brackets) {
                let _bracket = bracket.replace('{', '').replace('}', '');
                eval(``+eval_scopes+`;
                    _bracket = `+_bracket+`;
                 `)
                 parsed_value = parsed_value.replace(bracket, _bracket);
            }
            let element = <Node>style_binding.element;
            if(style_binding.element.getAttribute('style') !== parsed_value) {

                style_binding.element.setAttribute('style', parsed_value);
                style_binding.last_value = parsed_value;
            }
                
        }
    }

    eval_visible_condition() {
        if(!this.visible_condition)
            return true;

        let value = true,
            scopes = this.scopes,
            eval_scopes = this.eval_scopes(),
            expression = this.visible_condition.expression;

        eval(eval_scopes+`;
            value = `+expression+`;
        `);
        value ? this.show() : this.hide();
        return value;
    }


    create_scope(name) {
        let _cons = Declarations.scopes.filter((e) => e.prototype._karrot_name == name);
        if(_cons.length == 0)
            throw new Error('No scope with name '+name+'.');
        if(_cons.length > 1) 
            throw new Error('Duplicate scopes with name '+name+'.');

        let _con = _cons[0],
            scope_services = [],
            new_services = [],
            addons = [];
            
        if(_con.prototype.services)
        for(let service of _con.prototype.services) {
            let _service = this.services.filter((s) => s._karrot_name == service.prototype._karrot_name)[0];
            if(!_service) {
                _service = Reflect.construct(service, []);
                this.services.push(_service);
            }

            scope_services.push(_service);
        }
        if(_con.prototype.addons)
        for(let addon of _con.prototype.addons) {
            addons.push(Reflect.construct(addon, []));
        }
        let instance = <IScope>Reflect.construct(_con, [...scope_services, ...addons]);
        instance._karrot_init(UniqueValue.UniqueStorage,this);

        return instance;                
    }

    create_dynamic_scope(name:string, owner_name:string, data:Object):IScope {
        return {_karrot_name: name, _karrot_id: owner_name, data: data, is_dynamic: true};
    }

    add_scope(scope:IScope) {
        this.scopes.push(scope);
    }
    create_child(element:Element) {
        let child = new DataBinding(element, [...this.scopes], [...this.services], this.router);
        child.parent = this;
        this.children.push(child);
        return child;
    }
    create_loop_child(element:Element, expression:string) {
        let child = new LoopDataBinding(element, [...this.scopes], [...this.services], expression, this.router);
        child.parent = this;
        this.children.push(child);
        return child;
    }

    /*create_route_child(element:Element) {
        let child = new RouteDataBinding(element, [...this.scopes], [...this.services],  this.router);
        child.parent = this;
        this.children.push(child);
        return child;
    }*/

    

    hide() {
        if(this.is_visible) {
            if(!this.position_element) {
                this.position_element = document.createComment(' Karrot hidden element ');
                this.data_element.parentNode.insertBefore(this.position_element, this.data_element);
            }
            this.is_visible = false;
            this.data_element.parentNode.removeChild(this.data_element);
        }
    }

    show() {
        if(!this.is_visible) {
            this.is_visible = true;
            this.position_element.parentNode
                .insertBefore(this.data_element, 
                            this.position_element.nextSibling ? 
                            this.position_element.nextSibling : 
                            this.position_element);
                this.position_element.parentNode.removeChild(this.position_element);

            this.position_element = undefined;
        }
        
    }

    toogle() {
        this.is_visible ? this.hide() : this.show();
    }

    

    remove_local_scopes() {
        for(let scope of this.local_scopes) {
            if(scope.k_destroy)
                scope.k_destroy();

            this.scopes.splice(this.scopes.indexOf(scope), 1);
        }
    }

    remove() {
        this.data_element.innerHTML = '';

        //delete this.parent.children.splice(this.parent.children.indexOf(this), 1);
        //delete this;
    }

}


export class LoopDataBinding extends DataBinding {
    public owner_name:string;
    public fake_scope_name:string;
    public index_name:string;
    public pipes:Array<{type, expression:string}> = [];

    constructor(public data_element:Element, public scopes:Array<IScope>, public services:Array<IService>, public expression:string, public router:Router = undefined) {
        super(data_element, scopes, services,  router)

        this.position_element = document.createComment(' Karrot loop ');
        this.data_element.parentNode.insertBefore(this.position_element, this.data_element);
            
        
        this.data_element.parentNode.removeChild(this.data_element);

        
    }

    async init() {
        this.get_array_owner();
        this._parse();
    }
    _parse() {
        let array_item = this.loop_eval();
        array_item = this.check_sort(array_item);
        for(let _i in array_item) 
            this.add_loop_child(array_item[_i], _i);
        
    }

    change_detect(used_scopes:Array<string> = []) {
        console.log('change detect array', this.children_used_scope, used_scopes);
        if(!this.children_used_scope.some(e => used_scopes.indexOf(e) > -1 || used_scopes.length == 0))
            return;
        
        console.log('change detect array2');
        let array_item = this.loop_eval(),
            children_to_remove = [],
            index = 0;

        array_item = this.check_sort(array_item);

        //remove first rak
        for(let child of this.children) {
            let child_array = array_item.filter((e) => {
                return child.scopes.some((s) => {
                    return s.data == e ? true : false;
                });
            })
            if(child_array.length == 0)
                children_to_remove.push(child);
        }
        for(let child of children_to_remove)
            child.remove();
        

        for(let _i in array_item) {
            let child = this.children.filter((e) => {
                return e.scopes.some((s) => {
                    return s.data == array_item[_i] ? true : false;
                });
            })
            if(child.length == 0){
                console.log('add rak');
                this.add_loop_child(array_item[_i], index);
                index++;
            }
            else if(child[0].eval_visible_condition()) {
                child[0].scopes.filter((e) => e._karrot_name == this.index_name)[0].data = index;
                if(child[0] != this.children[_i]) {
                    this._set_position_of_element(child[0], index);
                }
                index++;
            }
            
        }

        
        for(let child of this.children) {
            child.change_detect(used_scopes);
        }
    }
    check_sort(array:Array<any>) {
        let sort_pipe = this.pipes.filter((p) => p.type == 'sort')[0];
        if(!sort_pipe)
            return array;
        
        let _sort_values = sort_pipe.expression.split('&&'),
            sort_values:Array<any> = [];
        for(let sort_value of _sort_values) {
            let sort_type = sort_value.indexOf('@asc') > -1 ? 'asc' : 'desc',
                sort_exp = sort_value.replace(/\s|@asc|@desc/gm, '');
            
            if(sort_exp.indexOf(this.fake_scope_name+'.') == 0)
                sort_exp = sort_exp.replace(this.fake_scope_name+'.', '');
            sort_values.push({exp: sort_exp, type: sort_type});
        }
        sort_values = sort_values.reverse();
        array.sort((a, b) => {
            for(let i = 0; i<sort_values.length;i++) {
                let f_val = sort_values[i],
                    n_val = sort_values[i+1],
                    f_v_a = f_val.type == 'asc' ? a[f_val.exp]:b[f_val.exp],
                    f_v_b = f_val.type == 'asc' ? b[f_val.exp]:a[f_val.exp];
                if(!n_val || a[n_val.exp] == b[n_val.exp])
                    if(isNaN(f_v_a))
                        return f_v_a.localeCompare(f_v_b); 
                    else 
                        return f_v_a-f_v_b
            }

        })
        return array;
    }
    add_loop_child(item, index:any) {
        let copy_element = <Element>this.data_element.cloneNode(true);
        copy_element.removeAttribute('*k:for');

        let child = this.create_child(copy_element);
        child.scopes.push(child.create_dynamic_scope(this.fake_scope_name, this.owner_name, item));
        child.scopes.push(child.create_dynamic_scope(this.index_name, this.owner_name, index));

        for(let pipe of this.pipes) {
            if(pipe.type == 'filter')
                child.visible_condition = new VisibleCondition(copy_element, child.scopes, pipe.expression);
        }
        this.set_position_of_element(copy_element, index);
        child.init();
    }

    set_position_of_element(element:Element, index:any) {
        
        let position_element = index > 0 && this.children[index-1] ? 
                                this.children[index-1].data_element.parentNode ? this.children[index-1].data_element : this.children[index-1].position_element : 
                                this.position_element;

        if(position_element.nextSibling)
            position_element.parentNode.insertBefore(element, position_element.nextSibling);
        else
            position_element.parentNode.appendChild(element);
    }

    _set_position_of_element(data_binding:DataBinding, index:any) {
        this.children.splice(this.children.indexOf(data_binding), 1);
        this.children.splice(index, 0, data_binding);

        this.set_position_of_element(data_binding.data_element, index);
    }

    get_array_owner() {
        let sections = this.expression.split('|'),
            exp = sections.shift().split('of'),
            scope_name = exp[0].replace(/\s/, ''),
            scope_array = exp[1].replace(/\s/, ''),
            owner = scope_array.split('.')[0],
            owner_id = this.scopes.filter((el) => el._karrot_name== owner)[0]._karrot_id;

        for(let pipe of sections) {
            
            let _pipe = pipe.split(':');
            this.pipes.push({type: _pipe[0].replace(/\W/gm, ''), expression: _pipe[1].replace(/\s/, '')});
        }

        this.owner_name = owner_id;
        if(scope_name.indexOf(',') != 0) {
            this.index_name = scope_name.split(',')[1];
            scope_name = scope_name.split(',')[0];
        }
        this.fake_scope_name = scope_name;
        this.used_scopes = this.parent.used_scopes;
    }

    loop_eval() {
        let sections = this.expression.split('|'),
            exp = sections.shift().split('of'),
            scope_name = exp[0].replace(/\s/, ''),
            scope_array = exp[1].replace(/\s/, ''),
            scope_data,
            scopes = this.scopes,
            eval_scopes = this.eval_scopes();
        eval(eval_scopes+`
            scope_data = `+scope_array+`;
        `)

        return scope_data;
    }
}
/*
Tworzyć DataBinding 

1. Stworzyć element, stworzyć DataBinding, dołączyć fake_scope
.*/

/*
export class RouteDataBinding extends DataBinding {
    constructor(public data_element:Element, public scopes:Array<IScope>, public services:Array<IService>,  public router:Router) {
        super(data_element, scopes, services)
    }

    init() {
        this.router.route_data_binding = this;
        this.create_route_scope();

        this._parse();
        this.hide_condition();
        this.eval_events();
        for(let child of this.children) 
            child.init();

        this.eval_text_nodes();
    }

    change_route(_pathname) {
        console.log('change route');
        this.remove_local_scopes();
        this.data_element.innerHTML = '';
        this.create_route_scope(_pathname);
    }

    create_route_scope(_pathname = '') {
        console.log('create new route');
        let {route_object, params} = this.router.get_route(_pathname),
            _con = route_object.route,
            scope_services = [],
            new_services = [];
            

        for(let service of _con.prototype.services) {
            let _service = this.services.filter((s) => s._karrot_name == service.prototype._karrot_name)[0];
            if(!_service) {
                _service = Reflect.construct(service, []);
                this.services.push(_service);
            }

            scope_services.push(_service);
        }
        let instance = <IRoute>Reflect.construct(_con, [...params, ...scope_services]);
        instance._karrot_init();

        this.data_element.innerHTML = instance.template;
        this.add_scope(instance);
        return instance;                
    }
}

*/