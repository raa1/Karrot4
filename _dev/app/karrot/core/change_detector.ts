import {DataManager} from './data_manager';


export class ChangeDetector {
    constructor(public data_manager:DataManager) {

    }

}







/*
change_detector
Każdy obiekt posiada change_detector, informuje on data_manager by
sprawdził zmiany w danych scopes.

nodes_manager
Posiada wszystkie nodes, które przekazuje data_manager, parsuje nodes, 
dodając odpowiednie przedraki (this.storages.user_storage) etc.

data_manager
Posiada wszystkie obiekty posiadające dane, oraz nodes_manager.
Updejtuje text_nodes.

user_storage.change_detector -> data_manager
data_manager.nodes_manager -> nodes
data_manager -> storages, nodes_manager

??
Każdy change_detector posiada data_manager, który zawiera sporo danych.
(optymalizacja?)


Tam gdzie tworze handlebars, potrzebuję obiektów z danymi.
   - NodesManager posiadający wszystkie obiekty i nodesy.
   {
       node: jakiś_tam_element_który_to_trzyma,
       need_update: true,
       binds: [
           {user_storage.username}
       ],
       last_value: ''
   }

   parse_node(element, scope) {
       //scope = {
           'user_storage': 'this.scopes["user_storage"]',
           'custom': 'scopes["user_storage"].custom_array[1]
       }
   }
   
   1. Sprawdzam zmianę porównując nowy tekst do 'last_value'.
   2. Każdy change detector posiada instancję NodesManager
   3. ChangeDetector "zleca" sprawdzenie nodesów tylko powiązanych cośków.//todo, scopes w ChangeDetector?.

   problemy
   Jeśli chcę tworzyć eventy w nodes_manager, muszę mieć dostęp do change_detectorów. (change_detector mają nodes_manager, nodes_manager change_detector)

   rozwiązania


*/