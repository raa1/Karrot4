export function KarrotInit(element) {
    let app = Reflect.construct(element, []);
    app.__karrot_init();
}