import {_Addon } from '../decorators/addon.decorator';

@_Addon({
    name: 'http',
    allow_services: true,
    allow_scopes: true,
    allow_app: true,
})
export class Http {
    constructor() {

    }

    get(url:string, params:Object = {}):Promise<XMLHttpRequest> {
        let params_string:String = '?';

        for(let _param in params) {
            params_string += _param+'='+params[_param];
            params_string += '&';
        }
        params_string = params_string.substring(0, params_string.length - 1);
        console.log(params_string);
        let promise = new Promise((resolve, reject) => {
            let req = new XMLHttpRequest();
            req.open("GET", url, true);
            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            req.onload = () => {
                resolve(req);
            };
            req.send(params_string);
        });
        return promise;
    }

    post(url:string, params:Object = {}) {
        let params_string:String = '';
        for(let _param in params) {
            params_string += _param+'='+params[_param];
            params_string += '&';
        }
        params_string = params_string.substring(0, params_string.length - 1);

            let promise = new Promise((resolve, reject) => {
                let req = new XMLHttpRequest();
                req.open("POST", url, true);
                req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                req.onload = () => {
                    if (req.status === 200) 
                        resolve(req.response);
                      else  reject(new Error(req.statusText));
                }
                req.send(params_string);
            });
            return promise;
    }
}