function _Pipe(data) {
    return function (constructor:Function) {
        constructor.prototype._karrot_name = data.name;
    };
}


@_Pipe({
    name: 'uppercase'
})
export class Uppercase {
    static transform(expression:string, params:Array<any>):string {
        console.log('params', params);
        return expression.toUpperCase();
    }
}