import {IKarrotObject} from './_karrot_object.interface';

export interface IService extends IKarrotObject{
    load_data();
    init();
}