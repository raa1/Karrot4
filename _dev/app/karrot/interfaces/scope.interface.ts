import {IKarrotObject} from './_karrot_object.interface';

import {IService} from './service.interface';
import {DataBinding} from '../core/data_binding';

export interface IScope extends IKarrotObject {
    is_dynamic?:boolean;
    data?:Object;

    _karrot_id:string;
    services?:Array<IService>;
    _karrot_init?(_id:string, data_binding:DataBinding);
    init?();
    load_data?();
    k_destroy?();

}