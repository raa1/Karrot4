import {IKarrotObject} from './_karrot_object.interface';

export interface IRoute extends IKarrotObject {
    route_element:Element;

    _karrot_init();
}