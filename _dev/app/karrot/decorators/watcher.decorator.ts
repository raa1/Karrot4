/*Karrot4 Watchers
watcher.decorator.ts 

Watchers allow you to watch method from other objects and execute watcher method with argument of return watched method. */

/* Creating method's decorator to watch other method @_Watch(Watched_Class, Watched_method). Apply to method. */
export function _Watch(watched_object: any, watched_function) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        if(watched_object.prototype.watchers[watched_function] == undefined)
            watched_object.prototype.watchers[watched_function] = [];

        if(target.watched_objects == undefined) 
            target.watched_objects = [];
        
        target.watched_objects.push({watched_object: watched_object, from: propertyKey, to: watched_function})

        //Adding method that will parse all watchers after init.
        target.__check_is_watching = function() {
            for(let _watched_object of this.watched_objects) {
                    _watched_object.watched_object.prototype.watchers[_watched_object.to].push({watcher: this,method: _watched_object.from});
            }

        }
        
    };
}

/*Creating class decorator that allow you to watch this class.*/
export function _Watchable() {
    return function(constructor: Function) {
        constructor.prototype.watchers = {};

        //Adding method that will replace all watched method.
        constructor.prototype.__check_is_watched = function() {
            for(let watch_method in this.watchers) {
                this['_'+watch_method] = this[watch_method];
                this[watch_method] = () => {
                    console.log('watch method', watch_method, this['_'+watch_method], this.watchers);
                    for(let watcher of this.watchers[watch_method]) {
                        watcher.watcher[watcher.method](this['_'+watch_method]());
                    }
                }
            }
        }
    };
}

