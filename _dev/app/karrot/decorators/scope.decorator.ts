import {ChangeDetector} from '../core/change_detector';
import {DataBinding} from '../core/data_binding';


export function _Scope(options) {
    return function(constructor: Function) {
        constructor.prototype._karrot_name = options.name;

        constructor.prototype.services = options.services;
        constructor.prototype.addons = options.addons;

        console.log(constructor.prototype.services)
        constructor.prototype._karrot_init = function(_id:string, data_binding:DataBinding) {
            this.change_detector = new ChangeDetector(data_binding);
            this._karrot_id = _id;
        }
    }
}
