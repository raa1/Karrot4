import {ChangeDetector} from '../core/change_detector';
import {DataBinding} from '../core/data_binding';


export function _Service(options) {
    return function(constructor: Function) {
        constructor.prototype._karrot_name = options.name;
        constructor.prototype.addons = options.addons;
        constructor.prototype._karrot_init = function() {

        }
    }
}
