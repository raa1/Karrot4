export function _Routing(options) {
    return function(constructor:Function) {
        constructor.prototype.routing_element = document.querySelector('*[__karrot_routing]');
        constructor.prototype.routings = options;

        window.onpopstate = (event) => {
            constructor.prototype.__karrot_routing();
        };

        constructor.prototype.__parse_links = function() {
            let routing_links = document.querySelectorAll('*[router-link]');
            for(let routing_link of routing_links) {
                //remove router-link attribute, add href, add onclick event
                let href = routing_link.getAttribute('router-link');
                routing_link.removeAttribute('router-link');
                routing_link.setAttribute('href', href);

                routing_link.addEventListener('click', (e) => {
                    e.preventDefault();
                    this.route_to(href);
                })

            }
        }

        constructor.prototype.__karrot_routing = function(_pathname = '') {
            //todo create instance of routing handler
            /*
                url: raadesign.pl/22_Karrot4/user/4
                template: user/:id 
                pathname: user/1

                check :
            */
            console.log('routing');
            if(this.router) {
                this.router.__karrot_route_delete();
                if(this.route.on_delete) {
                    this.route.on_delete();
                }
                delete this.router;
            }
            let pathname = _pathname != '' ? _pathname : document.location.pathname.replace(this.base_url, ''),
                split_pathname = pathname.split('/');

            for(let route_object of this.routings) {
                let route_pathname = route_object.pathname,
                    params = [];
                if(route_object.params) 
                    for(let route_param of route_object.params) {
                        route_pathname = route_pathname.replace(':'+route_param, '_karrot_param')
                    }
                        
                
                let split_route_pathname = route_pathname.split('/'),
                    i = 0;

                for(i = 0; i<split_pathname.length;i++) {
                    if(split_route_pathname[i] == '_karrot_param') {
                        params.push(split_pathname[i]);
                        continue;
                    }
                        
                    if(!split_route_pathname[i])
                        break;

                    if(split_pathname[i] != split_route_pathname[i])
                        break;
                }
                console.log(split_route_pathname, split_pathname, i);
                if(i == split_pathname.length) {
                    if(route_object.redirect_to){
                        this.__karrot_routing(route_object.redirect_to)
                        return;
                    }
                    let handler_requests = [],
                        handler_storages = [];
                        
                    if(route_object.handler) {
                        //Checking component's requests.
                        if(route_object.handler.prototype.requests != undefined)
                            for(let request of route_object.handler.prototype.requests) {
                                if(this.requests[request.prototype._karrot_name])
                                    handler_requests.push(this.requests[request.prototype._karrot_name]);
                            }

                        //Checking component's storages.
                        if(route_object.handler.prototype.storages != undefined)
                            for(let storage of route_object.handler.prototype.storages) {
                                if(this.storages[storage.prototype._karrot_name])
                                    handler_storages.push(this.storages[storage.prototype._karrot_name]);
                            }
                        console.log(handler_storages);
                        //todo requests, storages, event on_route_change of components
                        this.router = Reflect.construct(route_object.handler, [...params, ...handler_requests, ...handler_storages]);
                        this.router.app = this;
                        this.router.__karrot_init();
                    }

                    break;
                }

            }

        }

        /*constructor.prototype.route_to = function(pathname) {
            history.pushState({}, "Karrot", this.base_url+pathname);
            this.__karrot_routing();
        }*/
    }
}
