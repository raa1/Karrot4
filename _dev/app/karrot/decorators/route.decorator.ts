export function _Route(options) {
    return function(constructor:Function) {
        constructor.prototype._karrot_name = options.name;
        constructor.prototype.routing_element = document.querySelector('*[__karrot_routing]');
        constructor.prototype.services = options.services;
        constructor.prototype._karrot_init = function() {
            if(options.template) {
                this.template = options.template;
                //this.__karrot_create_template();
            }
                
            if(options.template_url) {
                let xhr= new XMLHttpRequest();
                xhr.open('GET', options.template_url, true);
                xhr.onload= () => {
                    this.template = xhr.responseText;
                    
                };
                xhr.send();
            }
        }

        constructor.prototype.__karrot_create_template = function() {
            this.routing_element.innerHTML = this.template;
            let objects = this.app.__create_components(this.routing_element);
            this.app.__init_objects(objects);
        }

        constructor.prototype.__karrot_route_delete = function() {
            this.routing_element.innerHTML = '';
            this.app.__clear_deprecated_components();
        }
    }
}