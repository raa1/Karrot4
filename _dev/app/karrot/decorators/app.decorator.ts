/*
Component // todo
*/
import {DataBinding} from '../core/data_binding';
import {Router} from '../core/router';
import {UniqueValue} from '../helpers/unique_value';

import {Declarations} from '../core/declarations';

export function _App(options) {
    return function(constructor:Function) {
        constructor.prototype._karrot_name = 'app';

        constructor.prototype.base_url = document.getElementsByTagName('base')[0].getAttribute('href');
        constructor.prototype.base_app_element = document.querySelector('*[__karrot_app]');

        constructor.prototype.__init_objects = function(all_objects) {
            //Call on_load function of all objects.
            for(let object of all_objects) {
                if(this.__karrot_routing)
                    object.router = this.router;
            }

            //Call __karrot_init of all objects.
            for(let object of all_objects) {
                if(object.__karrot_init != undefined)
                    object.__karrot_init();
            }
            
            //Check watching events of all objects.
            for(let object of all_objects) {
                if(object.__check_is_watching != undefined) {
                    object.__check_is_watching();
                }

            }
            for(let object of all_objects) {
                if(object.__check_is_watched != undefined) {
                    object.__check_is_watched();
                }
            }
            
            
            //Call on_load function of all objects.
            for(let object of all_objects) {
                if(object.on_load != undefined)
                    object.on_load();
            }
        }
        
        constructor.prototype.__karrot_init = function() {
            Declarations.init(options.declarations);
            console.log('declarations', [Declarations]);
            if(options.routing) {
                this.router = new Router(options.routing);
                
            }
            if(options.template) {
                this.template = options.template;
                this.__create_template();
            } else if(options.templateUrl) {
                let promise = new Promise((resolve, reject) => {
                    let req = new XMLHttpRequest();
                    req.open("GET", options.templateUrl, true);
                    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    req.onload = () => {
                        resolve(req.responseText);
                    };
                    req.send();
                });
                promise.then((res) => {
                    this.template = res;
                    this.__create_template();
                });
            }
                
        }

        constructor.prototype.create_services = function() {
            let services = [];
            for(let service_con of options.services) {
                let addons = [];
                if(service_con.prototype.addons) {
                    for(let addon of service_con.prototype.addons) {
                        addons.push(Reflect.construct(addon, []));
                    }
                }
                services.push(Reflect.construct(service_con, [...addons]));
            }
            return services;
        }

        constructor.prototype.create_global_scopes = function(services) {
            let scopes = [];
            for(let scope_con of options.scopes) {

                let scope_services = [];
                for(let service of scope_con.prototype.services) {

                    let _service = services.filter((s) => s._karrot_name == service.prototype._karrot_name)[0];

                    if(!_service) {
                        throw new Error('No service '+service.prototype._karrot_name+' in app decorator.');
                    }
                    scope_services.push(_service);
                }
                let instance = Reflect.construct(scope_con, [scope_services]);
                scopes.push(instance);
            }
            return scopes;
        }

        constructor.prototype.__create_template = function() {
            this.template = this.template.replace(/\{\s+/gm, '{');
            this.template = this.template.replace(/\s+\}/gm, '}');

            this._clone = this.base_app_element.cloneNode(true);
            this._clone.innerHTML = this.template;
            

            let services = this.create_services(),
                scopes = this.create_global_scopes(services);

            this.data_binding = new DataBinding(this._clone, scopes, services, this.router);
            for(let scope of scopes)
                scope._karrot_init(UniqueValue.UniqueStorage,this.data_binding);
            
            let load_promise = new Promise((resolve, reject) => {
                console.log('loaded dom0', document.readyState)
                if(document.readyState == 'complete') {
                    resolve();
                }
                document.onreadystatechange = function () {
                    if (document.readyState === "complete") {
                        resolve();
                    }
                }

            });
            Promise.all([this.data_binding.init(), load_promise]).then(() => {
                console.log('loaded');
                    this.base_app_element.parentElement.replaceChild(this._clone, this.base_app_element);
                    //this.base_app_element.replaceWith(this._clone);
                    this.base_app_element = this._clone;
                    setTimeout(() => {
                        this.base_app_element.classList.add('k-loaded');
                        if(this.init) {
                            this.init();
                        }
                    }, 1);
            });
            
            

        };
    };
}




