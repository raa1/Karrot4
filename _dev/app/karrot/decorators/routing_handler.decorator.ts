export function _RoutingHandler(options) {
    return function(constructor:Function) {
        constructor.prototype.storages = options.storages;
        constructor.prototype.requests = options.requests;

        constructor.prototype.routing_element = document.querySelector('*[__karrot_routing]');

        constructor.prototype.__karrot_init = function() {
            if(options.template) {
                this.template = options.template;
                this.__karrot_create_template();
            }
                
            if(options.template_url) {
                let xhr= new XMLHttpRequest();
                xhr.open('GET', options.template_url, true);
                xhr.onload= () => {
                    this.template = xhr.responseText;
                    this.__karrot_create_template();
                };
                xhr.send();
            }
        }

        constructor.prototype.__karrot_create_template = function() {
            this.routing_element.innerHTML = this.template;
            let objects = this.app.__create_components(this.routing_element);
            this.app.__init_objects(objects);
        }

        constructor.prototype.__karrot_route_delete = function() {
            this.routing_element.innerHTML = '';
            this.app.__clear_deprecated_components();
        }
    }
}