export class UniqueValue {
    static unique_storage_val:number = 0;
    static get UniqueStorage() {
        this.unique_storage_val++;
        return '__karrot_storage_'+this.unique_storage_val;
    }
}