export class BracketsHelper {
    static _brackets_regexp = /\{([^}]+)\}/gm;
    static _values_regexp = /\s*[{\+\-\*\/\}\(\)\]\[\,]\s*/gm;
    static _quotes_regexp = /".*?"|'.*?'/gm;


    static _brackets(text_string:string) {
        return text_string.match(this._brackets_regexp);
    }

    static _values(text_string:string) {
        return text_string.split(this._values_regexp);
    }

    //replacing/reversing quotes
    static _replace_quotes(text_string) {
        text_string = text_string.replace(this._quotes_regexp, '_karrot_text');

        return text_string;
    }

    static _reverse_quotes(text_string, quotes) {
        for(let quote in quotes) {
            text_string = text_string.replace(quote, quotes[quote]);
        }

        return text_string;
    }

    
}