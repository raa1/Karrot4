// App.ts
import { KarrotInit } from './karrot/core/init';
import { _App } from './karrot/decorators/app.decorator';
import {TasksService} from './services/tasks.service';
import {WeatherScope} from './scopes/weather.scope';

import {Uppercase} from './karrot/defined/uppercase.pipe';

@_App({
    templateUrl: 'templates/main.html',
    services: [TasksService],
    global_scopes: [],
    declarations: {
        scopes: [WeatherScope],
        pipes: [Uppercase]
    },
    scopes: []
})
export class App {
    constructor() {
    }
    init() {
    }
}

KarrotInit(App);

/*

*/