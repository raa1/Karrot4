import {_Scope} from '../karrot/decorators/scope.decorator';

import { Http } from '../karrot/defined/http.addon';
import { Cookies } from '../karrot/defined/cookies.addon';

@_Scope({
    name: 'weather',
    addons: [Http, Cookies]
})
export class WeatherScope {
    cities:Array<City> = [];
    search_city:string = '';
    constructor(public http:Http, public cookies:Cookies){

    }
    async load_data() {
        let cookies_info = this.cookies.get('cities').split(',');
        for(let cookie of cookies_info) {
            if(cookie != '')
                await this.add_city(cookie);
        }

    }
    async get_city(e) {
        await this.add_city(this.search_city);
        this.save_cookies();
        this.search_city = '';
    }

    async add_city(name) {
        console.log('name', name);
        let response = await this.http.get('http://api.openweathermap.org/data/2.5/forecast/daily?q='+name+'&APPID=48cff6357482b656af9be28f2e8fd9e4&units=metric'),
            weather =JSON.parse(response.responseText);
        
        console.log(weather)
        let city = new City(weather);
        for(let _city of this.cities) {
            if(_city.id == city.id) {
                this.cities.splice(this.cities.indexOf(_city), 1);
            }
        }
        this.cities.unshift(city);
    }

    remove_city(index) {
        this.cities.splice(index, 1);
        this.save_cookies();
    }
    save_cookies() {
        let cities_string = '';
        for(let city of this.cities) {
            cities_string += city.name+',';
        }

        this.cookies.set('cities', cities_string, 30);
    }

    
}

class City {
    public country:string;
    public name:string;
    public id:number;

    public currentDay:Day;
    public days:Array<Day> = [];
    constructor(api) {
        this.country = api.city.country;
        this.name = api.city.name;
        this.id = api.city.id;

        for(let api_day in api.list) {
            if(api_day <= 4)
                this.days.push(new Day(api.list[api_day]));
        }
        this.currentDay = this.days[0];
    }

    change_day(i) {
        this.currentDay = this.days[i];
    }
}

class Day {
    public day_temp:number;
    public night_temp:number;
    public humidity:number;
    public pressure:number;
    public clouds:number;
    public wind_speed:number;

    public weather_type:string;
    public weather_img:string;
    public time:Date;

    constructor(api) {
        this.day_temp = parseInt(api.temp.day);
        this.night_temp = parseInt(api.temp.night);
        this.humidity = api.humidity;
        this.pressure = api.pressure;
        this.clouds = api.clouds;
        this.wind_speed = parseInt(api.speed);

        this.time = new Date(api.dt*1000);

        switch(api.weather[0].main) {
            case 'Clear':
                this.weather_type = 'słonecznie';
                this.weather_img = 'sunny';
                break;
            case 'Snow':
            case 'Clouds':
                this.weather_type = "pochmurnie";
                this.weather_img = 'clouds';
                break;
            case 'Rain':
                this.weather_type = "deszczowo";
                this.weather_img = 'rain';
                break;
            default:
                this.weather_type = api.weather[0].main;
                this.weather_img = 'sunny';
        }
        
    }

    getDate() {
        return this.time.getDate()+'.'+(this.time.getMonth()+1)+'.'+this.time.getFullYear();
    }

    getDayOfWeek() {
        let days = ['niedziela', 'poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota'];
        return days[this.time.getDay()];
    }

}