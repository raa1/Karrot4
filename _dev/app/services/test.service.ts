import {_Service} from '../karrot/decorators/service.decorator';

@_Service({
    name: 'test_service'
})
export class TestService {
    constructor() {
        console.log('???');
    }
}