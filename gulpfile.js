var gulp = require('gulp'),
    ts = require('gulp-typescript'),
    uglify = require('gulp-uglify'),
    requirejs = require('gulp-requirejs'),
    uglify = require('gulp-uglify'),
    del = require('del'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    webserver = require('gulp-webserver');
 
gulp.task('ts-compile', function () {
    return gulp.src(['_dev/app/*.ts', '_dev/app/**/*.ts'])
        .pipe(ts({
            noImplicitAny: true,
            out: 'app.js',
            module: 'amd',
            target: 'es5',
            experimentalDecorators: true,
            isolatedModules: true
        }))
        .pipe(gulp.dest('_tstemp'));
});

gulp.task('sass', function () {
    return gulp
        .src('sass/chat.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/'));
});

gulp.task('amd-ts',['ts-compile'], function(){
  return requirejs({
    name: 'app',
    baseUrl: '_tstemp',
    out: 'app.js'
  })    
  //.pipe(uglify())
  .pipe(gulp.dest("dist"));
});

gulp.task('typescript', ['amd-ts'], function() {
  return del(['_tstemp']);
});

gulp.task('default', function() {
    gulp.src('./')
        .pipe(webserver({
        livereload: {
            path: '/',
            fallback: './index.html',
            enable: true, // need this set to true to enable livereload 
            filter: function(fileName) {
                if (fileName.indexOf('_dev') > 0 || fileName.indexOf('_tstemp') > 0|| fileName.indexOf('node_modules') > 0) {
                    return false;
                } else {
                    return true;
                }
            }
        },
        directoryListing: true,
        open: true
}));
    //gulp.watch(['_dev/sass/*.scss', '_dev/sass/*/*.scss'], ['sass']);
    gulp.watch(['_dev/app/*.ts','_dev/app/**/*.ts'], ['amd-ts']);
    gulp.watch(['sass/*.scss'], ['sass']);
})